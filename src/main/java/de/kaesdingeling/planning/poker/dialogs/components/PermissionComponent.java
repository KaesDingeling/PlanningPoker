package de.kaesdingeling.planning.poker.dialogs.components;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;

import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.data.language.parameters.Dialog_LanguageParameters;
import de.kaesdingeling.planning.poker.data.models.PokerData;
import lombok.Getter;

/**
 * 
 * @created 10.07.2020 - 00:48:43
 * @author KaesDingeling
 * @version 0.1
 */
public class PermissionComponent extends VerticalLayout implements HasLanguage {
	private static final long serialVersionUID = 5653025971064958387L;
	
	// Layout
	private H4 title;
	private Checkbox allCanWriteName;
	private Checkbox allCanShowVotes;
	private Checkbox allCanReset;
	private Checkbox allCanKick;
	
	// Data
	@Getter
	private Binder<PokerData> binder;
	
	/**
	 * 
	 * @param data
	 */
	public PermissionComponent(PokerData data) {
		super();
		
		binder = new Binder<PokerData>();
		binder.setBean(data);
		
		title = new H4();
		title.getStyle().set("margin", "0 0 20px");
		
		allCanWriteName = new Checkbox();
		allCanShowVotes = new Checkbox();
		allCanReset = new Checkbox();
		allCanKick = new Checkbox();
		
		binder.forField(allCanWriteName).bind(PokerData::isAllCanWriteName, PokerData::setAllCanWriteName);
		binder.forField(allCanShowVotes).bind(PokerData::isAllCanShowVotes, PokerData::setAllCanShowVotes);
		binder.forField(allCanReset).bind(PokerData::isAllCanReset, PokerData::setAllCanReset);
		binder.forField(allCanKick).bind(PokerData::isAllCanKick, PokerData::setAllCanKick);
		
		add(title, allCanWriteName, allCanShowVotes, allCanReset, allCanKick);
		getStyle().set("margin-top", "0");
		setSpacing(false);
	}

	@Override
	public void updateLanguages() {
		title.setText(UITextUtils.text(Dialog_LanguageParameters.CreateSession.Permission.permission));
		
		allCanWriteName.setLabel(UITextUtils.text(Dialog_LanguageParameters.CreateSession.Permission.all_can_write_name));
		allCanShowVotes.setLabel(UITextUtils.text(Dialog_LanguageParameters.CreateSession.Permission.all_can_show_votes));
		allCanReset.setLabel(UITextUtils.text(Dialog_LanguageParameters.CreateSession.Permission.all_can_reset));
		allCanKick.setLabel(UITextUtils.text(Dialog_LanguageParameters.CreateSession.Permission.all_can_kick));
	}
}