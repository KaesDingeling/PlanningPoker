package de.kaesdingeling.planning.poker.dialogs.components;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Result;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.converter.Converter;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.dom.Style;

import de.kaesdingeling.commons.components.fontawesome.icons.FontAwesomeSolid;
import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.Dialog_LanguageParameters;
import de.kaesdingeling.planning.poker.data.models.PokerOptionData;
import lombok.Getter;

/**
 * 
 * @created 18.06.2020 - 23:46:36
 * @author KaesDingeling
 * @version 0.1
 */
public class ConfigItemComponent extends HorizontalLayout implements HasLanguage {
	private static final long serialVersionUID = -653223296103659812L;
	
	// Layout
	private TextField nameField;
	private NumberField valueField;
	private Button moveUp;
	private Button moveDown;
	private Button remove;
	
	// Data
	@Getter
	private Binder<PokerOptionData> binder;
	private ConfigComponent mainLayout;
	
	/**
	 * 
	 * @param mainLayout
	 * @param data
	 */
	public ConfigItemComponent(ConfigComponent mainLayout, PokerOptionData data) {
		super();
		
		this.mainLayout = mainLayout;
		
		nameField = new TextField();
		nameField.setValueChangeMode(ValueChangeMode.EAGER);
		nameField.setWidthFull();
		
		valueField = new NumberField();
		valueField.setValueChangeMode(ValueChangeMode.EAGER);
		valueField.setHasControls(true);
		valueField.setWidth("120px");
		
		
		binder = new Binder<PokerOptionData>();
		binder.forField(nameField).bind(PokerOptionData::getName, PokerOptionData::setName);
		binder.forField(valueField)
				.withConverter(new Converter<Double, Integer>() {
					private static final long serialVersionUID = 7771281044423731450L;

					@Override
					public Result<Integer> convertToModel(Double value, ValueContext context) {
						if (value != null) {
							return Result.ok(value.intValue());
						}
						
						return Result.error("Failed to convert");
					}

					@Override
					public Double convertToPresentation(Integer value, ValueContext context) {
						if (value != null) {
							return value.doubleValue();
						}
						
						return null;
					}
				})
				.bind(PokerOptionData::getValue, PokerOptionData::setValue);
		binder.setBean(data);
		binder.addValueChangeListener(e -> mainLayout.update());
		
		
		moveUp = createButton(FontAwesomeSolid.ARROW_UP.create(), e -> mainLayout.moveUp(this));
		moveDown = createButton(FontAwesomeSolid.ARROW_DOWN.create(), e -> mainLayout.moveDown(this));
		remove = createButton(FontAwesomeSolid.TRASH.create(), e -> mainLayout.remove(this));
		
		setAlignItems(Alignment.END);
		add(nameField, valueField, moveUp, moveDown, remove);
		expand(nameField);
		
		updateLanguages();
		update();
	}
	
	/**
	 * 
	 * 
	 * @Created 19.06.2020 - 12:24:43
	 * @author KaesDingeling
	 */
	public void update() {
		Style style = getStyle();
		
		if (isEmpty()) {
			moveUp.setEnabled(false);
			moveDown.setEnabled(false);

			style.set("border", "1px dashed var(--lumo-body-text-color)");
			style.set("width", "calc(100% + 20px)");
			style.set("border-radius", "5px");
			style.set("position", "relative");
			style.set("margin-top", "10px");
			style.set("padding", "1px 5px");
			style.set("left", "-10px");
			
			if (!remove.getThemeNames().contains(ButtonVariant.LUMO_SUCCESS.getVariantName())) {
				remove.getThemeNames().set(ButtonVariant.LUMO_SUCCESS.getVariantName(), true);
				remove.setIcon(FontAwesomeSolid.PLUS.create());
				remove.setEnabled(false);
			}
		} else {
			moveUp.setEnabled(mainLayout.isMoveUpAllowed(this));
			moveDown.setEnabled(mainLayout.isMoveDownAllowed(this));
			
			style.set("width", "100%");
			style.remove("border-radius");
			style.remove("margin-top");
			style.remove("position");
			style.remove("border");
			style.remove("left");
			
			if (!remove.getThemeNames().contains(ButtonVariant.LUMO_ERROR.getVariantName())) {
				remove.getThemeNames().set(ButtonVariant.LUMO_ERROR.getVariantName(), true);
				remove.setIcon(FontAwesomeSolid.TRASH.create());
				remove.setEnabled(true);
			}
		}
	}
	
	/**
	 * 
	 * @return
	 * @Created 19.06.2020 - 12:39:55
	 * @author KaesDingeling
	 */
	public boolean isEmpty() {
		return (StringUtils.isBlank(getBinder().getBean().getName()) || getBinder().getBean().getValue() == 0);
	}
	
	@Override
	public void updateLanguages() {
		nameField.setPlaceholder(UITextUtils.text(ELanguageSuffix.Placeholder.getName(), Dialog_LanguageParameters.CreateSession.Config.input_name));
		valueField.setPlaceholder(UITextUtils.text(ELanguageSuffix.Placeholder.getName(), Dialog_LanguageParameters.CreateSession.Config.input_value));
	}
	
	/**
	 * 
	 * @param icon
	 * @param listener
	 * @return
	 * @Created 19.06.2020 - 12:25:45
	 * @author KaesDingeling
	 */
	private Button createButton(Component icon, ComponentEventListener<ClickEvent<Button>> listener) {
		Button button = new Button(icon);
		button.addClickListener(listener);
		button.addThemeVariants(ButtonVariant.LUMO_ICON);
		
		return button;
	}
}