package de.kaesdingeling.planning.poker.dialogs.components;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.google.common.collect.Sets;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.data.language.parameters.Dialog_LanguageParameters;
import de.kaesdingeling.planning.poker.data.models.PokerOptionData;
import lombok.Getter;

/**
 * 
 * @created 18.06.2020 - 23:52:51
 * @author KaesDingeling
 * @version 0.1
 */
public class ConfigComponent extends VerticalLayout implements HasLanguage {
	private static final long serialVersionUID = -7561561160218300731L;
	
	// Header
	private H4 title;
	private HorizontalLayout header;
	private Label nameTitle;
	private Label valueTitle;
	
	private VerticalLayout content;
	
	// Data
	@Getter
	private Set<PokerOptionData> orginalOptions;
	
	/**
	 * 
	 * @param options
	 * @param consumer
	 */
	public ConfigComponent(Set<PokerOptionData> options, Consumer<Set<PokerOptionData>> consumer) {
		super();
		
		this.orginalOptions = options;
		
		if (options == null) {
			this.orginalOptions = Sets.newCopyOnWriteArraySet();
		}
		
		title = new H4();
		title.getStyle().set("margin-bottom", "20px");
		
		nameTitle = new Label();
		valueTitle = new Label();
		valueTitle.setWidth("277px");
		
		header = new HorizontalLayout();
		header.add(nameTitle, valueTitle);
		header.expand(nameTitle);
		header.setWidthFull();
		
		content = new VerticalLayout();
		content.setPadding(false);
		content.setSpacing(false);
		
		add(title, header, content);
		
		getStyle().set("padding-top", "0");
		getStyle().set("margin-top", "0");
		
		setSpacing(false);
		
		reset();
	}
	
	@Override
	public void updateLanguages() {
		title.setText(UITextUtils.text(Dialog_LanguageParameters.CreateSession.Config.config));
		
		nameTitle.setText(UITextUtils.text(Dialog_LanguageParameters.CreateSession.Config.input_name));
		valueTitle.setText(UITextUtils.text(Dialog_LanguageParameters.CreateSession.Config.input_value));
	}
	
	/**
	 * 
	 * @param item
	 * @Created 19.06.2020 - 12:17:05
	 * @author KaesDingeling
	 */
	public void remove(ConfigItemComponent item) {
		content.remove(item);
		update();
	}
	
	/**
	 * 
	 * @param item
	 * @Created 19.06.2020 - 12:17:26
	 * @author KaesDingeling
	 */
	public void moveUp(ConfigItemComponent item) {
		int currentPosition = getCurrentPosition(item);
		
		if (currentPosition > 0) {
			content.remove(item);
			content.addComponentAtIndex((currentPosition - 1), item);
			update();
		}
	}
	
	/**
	 * 
	 * @param item
	 * @Created 19.06.2020 - 12:19:49
	 * @author KaesDingeling
	 */
	public void moveDown(ConfigItemComponent item) {
		int currentPosition = getCurrentPosition(item);

		update();
		if (currentPosition >= 0 && (currentPosition + 1) <= (content.getComponentCount() - 2)) {
			content.remove(item);
			content.addComponentAtIndex((currentPosition + 1), item);
			update();
		}
	}
	
	/**
	 * 
	 * @param item
	 * @return
	 * @Created 19.06.2020 - 12:24:30
	 * @author KaesDingeling
	 */
	public boolean isMoveUpAllowed(ConfigItemComponent item) {
		int currentPosition = getCurrentPosition(item);
		
		return (currentPosition > 0);
	}
	
	/**
	 * 
	 * @param item
	 * @return
	 * @Created 19.06.2020 - 12:24:03
	 * @author KaesDingeling
	 */
	public boolean isMoveDownAllowed(ConfigItemComponent item) {
		int currentPosition = getCurrentPosition(item);
		
		return (currentPosition >= 0 && (currentPosition + 1) <= (content.getComponentCount() - 2));
	}
	
	/**
	 * 
	 * @param item
	 * @return
	 * @Created 19.06.2020 - 12:20:18
	 * @author KaesDingeling
	 */
	public int getCurrentPosition(ConfigItemComponent item) {
		int currentPosition = -1;
		
		for (int i = 0; i < content.getComponentCount(); i++) {
			if (content.getComponentAt(i).equals(item)) {
				currentPosition = i;
			}
		}
		
		return currentPosition;
	}
	
	/**
	 * 
	 * 
	 * @Created 19.06.2020 - 00:28:59
	 * @author KaesDingeling
	 */
	public void reset() {
		content.removeAll();
		
		if (CollectionUtils.isNotEmpty(orginalOptions)) {
			for (PokerOptionData option : orginalOptions) {
				content.add(new ConfigItemComponent(this, option));
			}
		}
		
		update();
	}
	
	/**
	 * 
	 * 
	 * @Created 19.06.2020 - 10:54:49
	 * @author KaesDingeling
	 */
	public void update() {
		boolean emptyOptionAvalible = false;
		
		for (ConfigItemComponent item : getComponetItems()) {
			item.update();
			
			if (item.isEmpty()) {
				emptyOptionAvalible = true;
				break;
			} else {
				item.getStyle().remove("padding");
			}
		}
		
		if (!emptyOptionAvalible) {
			content.add(new ConfigItemComponent(this, PokerOptionData.builder().build()));
			update();
		}
	}
	
	/**
	 * 
	 * @return
	 * @Created 19.06.2020 - 12:21:31
	 * @author KaesDingeling
	 */
	public List<ConfigItemComponent> getComponetItems() {
		return content.getChildren()
				.filter(Objects::nonNull)
				.filter(c -> c instanceof ConfigItemComponent)
				.map(c -> (ConfigItemComponent) c)
				.collect(Collectors.toList());
	}
	
	/**
	 * 
	 * @return
	 * @Created 18.06.2020 - 23:56:26
	 * @author KaesDingeling
	 */
	public List<PokerOptionData> getOptions() {
		return getComponetItems()
				.stream()
				.map(i -> i.getBinder().getBean())
				.collect(Collectors.toList());
	}
}