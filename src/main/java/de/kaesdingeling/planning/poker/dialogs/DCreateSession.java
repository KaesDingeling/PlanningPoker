package de.kaesdingeling.planning.poker.dialogs;

import javax.servlet.http.Cookie;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinResponse;
import com.vaadin.flow.server.VaadinService;

import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.Application;
import de.kaesdingeling.planning.poker.data.constants.CONSTANTS;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.Dialog_LanguageParameters;
import de.kaesdingeling.planning.poker.data.models.PokerData;
import de.kaesdingeling.planning.poker.dialogs.components.ConfigComponent;
import de.kaesdingeling.planning.poker.dialogs.components.ConfigItemComponent;
import de.kaesdingeling.planning.poker.dialogs.components.PermissionComponent;
import de.kaesdingeling.planning.poker.services.SessionService;
import de.kaesdingeling.planning.poker.utils.PokerUtils;
import de.kaesdingeling.planning.poker.views.VPoker;

/**
 * 
 * @created 14.06.2020 - 19:34:32
 * @author KaesDingeling
 * @version 0.1
 */
public class DCreateSession extends Dialog implements HasLanguage {
	private static final long serialVersionUID = 8436636977792534444L;
	
	private VerticalLayout content;
	private H3 header;
	private TextField usernameField;
	private Button createButton;
	private ConfigComponent config;
	private PermissionComponent permissions;
	
	private PokerData data;
	
	/**
	 * 
	 */
	public DCreateSession() {
		super();
		
		data = PokerUtils.createSession();
		
		header = new H3();
		
		usernameField = new TextField();
		usernameField.setValueChangeMode(ValueChangeMode.EAGER);
		usernameField.addValueChangeListener(e -> refresh());
		usernameField.setWidthFull();
		
		config = new ConfigComponent(data.getOptions(), i -> refresh());
		config.setWidthFull();
		
		permissions = new PermissionComponent(data);
		permissions.setWidthFull();
		
		createButton = new Button();
		createButton.addClickListener(e -> submit());
		createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		
		content = new VerticalLayout();
		content.setAlignItems(Alignment.START);
		content.add(header, usernameField, config, permissions, createButton);
		content.setAlignSelf(Alignment.END, createButton);
		content.setMinWidth("400px");
		content.setPadding(false);
		content.setSizeFull();
		
		add(content);
		
		refresh();
		
		setCloseOnOutsideClick(false);
		
		open();
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 20:33:46
	 * @author KaesDingeling
	 */
	public void submit() {
		VaadinResponse response = VaadinService.getCurrentResponse();
		
		if (response != null) {
			Cookie cookie = new Cookie(CONSTANTS.COOKIE_USERNAME, usernameField.getValue());
			
			cookie.setMaxAge(Integer.MAX_VALUE);
			
			response.addCookie(cookie);
		}
		
		UI ui = UI.getCurrent();
		
		data.getOptions().clear();
		
		for (ConfigItemComponent configItem : config.getComponetItems()) {
			if (!configItem.isEmpty()) {
				data.getOptions().add(configItem.getBinder().getBean());
			}
		}
		
		if (Application.getBean(SessionService.class).addSession(data)) {
			ui.getSession().setAttribute(CONSTANTS.COOKIE_USERNAME, usernameField.getValue());
			ui.navigate(VPoker.class, data.getUuid().toString());
			
			close();
		} else {
			Notification notification = new Notification("Error", 5000, Position.BOTTOM_CENTER);
			
			//notification.setText(UITextUtils.text(View_LanguageParameters.Pocker.not_found, uuid.toString()));
			notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
			notification.open();
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 20:06:05
	 * @author KaesDingeling
	 */
	public void refresh() {
		if (StringUtils.isBlank(usernameField.getValue())) {
			VaadinRequest request = VaadinService.getCurrentRequest();
			
			if (request != null) {
				Cookie[] cookies = request.getCookies();
				
				if (ArrayUtils.isNotEmpty(cookies)) {
					for (Cookie cookie : cookies) {
						if (StringUtils.equals(cookie.getName(), CONSTANTS.COOKIE_USERNAME)) {
							usernameField.setValue(cookie.getValue());
							break;
						}
					}
				}
			}
		}
		
		createButton.setEnabled((StringUtils.isNotBlank(usernameField.getValue()) && CollectionUtils.isNotEmpty(config.getOptions())));
	}

	@Override
	public void updateLanguages() {
		header.setText(UITextUtils.text(Dialog_LanguageParameters.CreateSession.create_session));
		
		createButton.setText(UITextUtils.text(Dialog_LanguageParameters.CreateSession.create));
		
		usernameField.setLabel(UITextUtils.text(Dialog_LanguageParameters.CreateSession.username));
		usernameField.setPlaceholder(UITextUtils.text(ELanguageSuffix.Placeholder.getName(), Dialog_LanguageParameters.CreateSession.username));
	}
}