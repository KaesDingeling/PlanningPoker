package de.kaesdingeling.planning.poker.dialogs;

import java.util.UUID;

import javax.servlet.http.Cookie;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinResponse;
import com.vaadin.flow.server.VaadinService;

import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.data.constants.CONSTANTS;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.Dialog_LanguageParameters;
import de.kaesdingeling.planning.poker.data.models.PokerData;
import de.kaesdingeling.planning.poker.utils.PokerUtils;
import de.kaesdingeling.planning.poker.views.VPoker;

/**
 * 
 * @created 14.06.2020 - 19:34:32
 * @author KaesDingeling
 * @version 0.1
 */
public class DJoinSession extends Dialog implements HasLanguage {
	private static final long serialVersionUID = 8436636977792534444L;
	
	private VerticalLayout layout;
	private H3 title;
	private TextField idField;
	private TextField usernameField;
	private Button joinButton;
	
	/**
	 * 
	 */
	public DJoinSession() {
		this("");
	}
	
	/**
	 * 
	 * @param sessionID
	 */
	public DJoinSession(String sessionID) {
		super();
		
		title = new H3();
		
		usernameField = new TextField();
		usernameField.setValueChangeMode(ValueChangeMode.EAGER);
		usernameField.addValueChangeListener(e -> e.getSource().setInvalid(StringUtils.isBlank(e.getValue())));
		usernameField.addValueChangeListener(e -> update());
		usernameField.setInvalid(true);
		usernameField.setWidthFull();
		usernameField.focus();
		
		idField = new TextField();
		idField.setValueChangeMode(ValueChangeMode.EAGER);
		idField.addValueChangeListener(e -> e.getSource().setInvalid(PokerUtils.parseID(e.getValue()) == null));
		idField.addValueChangeListener(e -> update());
		idField.setInvalid(true);
		idField.setWidthFull();
		
		joinButton = new Button();
		joinButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		joinButton.addClickListener(e -> submit());
		joinButton.setEnabled(false);
		
		layout = new VerticalLayout();
		layout.add(title, usernameField, idField, joinButton);
		layout.setAlignSelf(Alignment.END, joinButton);
		layout.setMinWidth("350px");
		layout.setSizeFull();

		idField.setValue(sessionID);
		
		add(layout);
		
		refresh();
		
		setCloseOnOutsideClick(false);
		
		open();
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 20:47:49
	 * @author KaesDingeling
	 */
	public void update() {
		joinButton.setEnabled((!idField.isInvalid() && !usernameField.isInvalid() && PokerUtils.parseID(idField.getValue()) != null && StringUtils.isNoneBlank(usernameField.getValue())));
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 20:33:46
	 * @author KaesDingeling
	 */
	public void submit() {
		VaadinResponse response = VaadinService.getCurrentResponse();
		
		if (response != null) {
			Cookie cookie = new Cookie(CONSTANTS.COOKIE_USERNAME, usernameField.getValue());
			
			cookie.setMaxAge(Integer.MAX_VALUE);
			
			response.addCookie(cookie);
		}
		
		UUID id = PokerUtils.parseID(idField.getValue());
		PokerData data = null;
		
		if (id != null) {
			data = PokerUtils.searchSessionById(id);
		}
		
		if (data != null) {
			UI ui = UI.getCurrent();
			
			ui.getSession().setAttribute(CONSTANTS.COOKIE_USERNAME, usernameField.getValue());
			ui.navigate(VPoker.class, data.getUuid().toString());
			
			close();
		} else {
			Notification notification = new Notification("Error", 5000, Position.BOTTOM_CENTER);
			
			//notification.setText(UITextUtils.text(View_LanguageParameters.Pocker.not_found, uuid.toString()));
			notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
			notification.open();
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 20:06:05
	 * @author KaesDingeling
	 */
	public void refresh() {
		if (StringUtils.isBlank(usernameField.getValue())) {
			VaadinRequest request = VaadinService.getCurrentRequest();
			
			if (request != null) {
				Cookie[] cookies = request.getCookies();
				
				if (ArrayUtils.isNotEmpty(cookies)) {
					for (Cookie cookie : cookies) {
						if (StringUtils.equals(cookie.getName(), CONSTANTS.COOKIE_USERNAME)) {
							usernameField.setValue(cookie.getValue());
							break;
						}
					}
				}
			}
		}
	}

	@Override
	public void updateLanguages() {
		title.setText(UITextUtils.text(Dialog_LanguageParameters.JoinSession.join_session));
		idField.setLabel(UITextUtils.text(Dialog_LanguageParameters.JoinSession.id));
		idField.setPlaceholder(UITextUtils.text(ELanguageSuffix.Placeholder.getName(), Dialog_LanguageParameters.JoinSession.id));
		usernameField.setLabel(UITextUtils.text(Dialog_LanguageParameters.JoinSession.username));
		usernameField.setPlaceholder(UITextUtils.text(ELanguageSuffix.Placeholder.getName(), Dialog_LanguageParameters.JoinSession.username));
		joinButton.setText(UITextUtils.text(Dialog_LanguageParameters.JoinSession.join));
	}
}