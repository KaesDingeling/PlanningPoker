package de.kaesdingeling.planning.poker.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;

import de.kaesdingeling.commons.components.fontawesome.FontAwesomeIcon;
import de.kaesdingeling.commons.components.fontawesome.icons.FontAwesomeSolid;
import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.data.language.parameters.View_LanguageParameters;
import de.kaesdingeling.planning.poker.dialogs.DCreateSession;
import de.kaesdingeling.planning.poker.dialogs.DJoinSession;
import de.kaesdingeling.planning.poker.views.router.RMainLayout;

/**
 * 
 * @created 14.06.2020 - 12:56:37
 * @author KaesDingeling
 * @version 0.1
 */
@PreserveOnRefresh
@Route(value = VMain.URL, layout = RMainLayout.class)
public class VMain extends VerticalLayout implements LocaleChangeObserver {
	private static final long serialVersionUID = 6305046174551321430L;
	
	public static final String URL = "";
	
	private FontAwesomeIcon icon;
	private H1 title;
	private H4 descrption;
	
	private HorizontalLayout selectLayout;
	private Button createNewSession;
	private Button joinSession;
	
	/**
	 * 
	 */
	public VMain() {
		super();
		
		icon = FontAwesomeSolid.DICE.create();
		icon.getStyle().set("text-align", "center");
		icon.getStyle().set("font-size", "50px");
		icon.getStyle().set("height", "40px");
		icon.getStyle().set("width", "100px");
		
		title = new H1();
		title.getStyle().set("margin-bottom", "20px");
		
		descrption = new H4();
		descrption.getStyle().set("margin", "0 0 20px");
		descrption.setVisible(false);
		
		createNewSession = createButton();
		createNewSession.addClickListener(e -> new DCreateSession().open());
		
		joinSession = createButton();
		joinSession.addClickListener(e -> new DJoinSession().open());
		
		selectLayout = new HorizontalLayout();
		selectLayout.add(createNewSession, joinSession);
		selectLayout.getStyle().set("margin-bottom", "78px");
		
		setJustifyContentMode(JustifyContentMode.CENTER);
		setAlignItems(Alignment.CENTER);
		add(icon, title, descrption, selectLayout);
		setSizeFull();
		
		//updateLanguages();
	}
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 13:03:14
	 * @author KaesDingeling
	 */
	private Button createButton() {
		Button button = new Button();
		
		button.addThemeVariants(ButtonVariant.LUMO_LARGE);
		button.getStyle().set("padding", "20px 40px 25px");
		button.getStyle().set("font-size", "30px");
		button.setHeight("100px");
		button.setWidth("200px");
		
		return button;
	}

	/*
	@Override
	public void updateLanguages() {
		String titleText = UITextUtils.text(View_LanguageParameters.Main.main);
		
		UI.getCurrent().getPage().setTitle(titleText);
		
		title.setText(titleText);
		//descrption.setText(UITextUtils.text(ELanguageSuffix.Descrption.getName(), View_LanguageParameters.Main.main));
		
		createNewSession.setText(UITextUtils.text(View_LanguageParameters.Main.button_create));
		joinSession.setText(UITextUtils.text(View_LanguageParameters.Main.button_join));
	}
	*/

	@Override
	public void localeChange(final LocaleChangeEvent event) {
		String titleText = getTranslation(View_LanguageParameters.Main.main.getPrefix() + "." + View_LanguageParameters.Main.main.getFull(), "nö", "te", "na");
		
		UI.getCurrent().getPage().setTitle(titleText);
		
		title.setText(titleText);
		
		createNewSession.setText(getTranslation(View_LanguageParameters.Main.button_create.getFull()));
		joinSession.setText(getTranslation(View_LanguageParameters.Main.button_create.getFull()));
	}
}