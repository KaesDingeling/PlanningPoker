package de.kaesdingeling.planning.poker.views.components;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.vaadin.flow.component.html.Div;

import de.kaesdingeling.planning.poker.data.models.PokerOptionData;
import de.kaesdingeling.planning.poker.views.VPoker;
import lombok.Getter;

/**
 * 
 * @created 19.06.2020 - 13:24:08
 * @author KaesDingeling
 * @version 0.1
 */
public class PokerOptionsComponent extends Div {
	private static final long serialVersionUID = 1245096879530643109L;
	
	@Getter
	private VPoker view;

	/**
	 * 
	 * @param view
	 */
	public PokerOptionsComponent(VPoker view) {
		super();
		
		this.view = view;
		
		for (PokerOptionData option : view.getSessionConfig().getOptions()) {
			add(new PokerOptionsOptionComponent(this, option));
		}
		
		setWidthFull();
	}
	
	/**
	 * 
	 * 
	 * @Created 10.07.2020 - 00:24:18
	 * @author KaesDingeling
	 */
	public void update() {
		getComponetItems().forEach(option -> option.update());
	}
	
	/**
	 * 
	 * @return
	 * @Created 19.06.2020 - 12:21:31
	 * @author KaesDingeling
	 */
	public List<PokerOptionsOptionComponent> getComponetItems() {
		return getChildren()
				.filter(Objects::nonNull)
				.filter(c -> c instanceof PokerOptionsOptionComponent)
				.map(c -> (PokerOptionsOptionComponent) c)
				.collect(Collectors.toList());
	}
	
	/**
	 * 
	 * @return
	 * @Created 18.06.2020 - 23:56:26
	 * @author KaesDingeling
	 */
	public List<PokerOptionData> getOptions() {
		return getComponetItems()
				.stream()
				.map(i -> i.getData())
				.collect(Collectors.toList());
	}
}