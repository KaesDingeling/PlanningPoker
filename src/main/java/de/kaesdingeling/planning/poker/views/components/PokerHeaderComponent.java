package de.kaesdingeling.planning.poker.views.components;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;

import de.kaesdingeling.commons.components.fontawesome.icons.FontAwesomeSolid;
import de.kaesdingeling.commons.ui.utils.UIUtils;
import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.View_LanguageParameters;
import de.kaesdingeling.planning.poker.views.VPoker;
import lombok.Getter;

/**
 * 
 * @created 09.07.2020 - 23:33:21
 * @author KaesDingeling
 * @version 0.1
 */
@Getter
public class PokerHeaderComponent extends HorizontalLayout implements HasLanguage {
	private static final long serialVersionUID = 5723192055721667553L;
	
	// Layout
	private TextField nameField;
	private Button clearVotesButton;
	private Button showVotesButton;
	
	// Data
	private VPoker view;
	
	/**
	 * 
	 * @param view
	 */
	public PokerHeaderComponent(VPoker view) {
		super();
		
		this.view = view;
		
		nameField = new TextField();
		nameField.setWidthFull();
		nameField.setValueChangeTimeout(500);
		nameField.setValueChangeMode(ValueChangeMode.TIMEOUT);
		nameField.addValueChangeListener(e -> {
			view.getSessionConfig().setName(e.getValue());
			view.preUpdateExcludeOwn();
		});
		
		clearVotesButton = new Button();
		clearVotesButton.setIcon(FontAwesomeSolid.ERASER.create());
		clearVotesButton.addClickListener(e -> {
			view.getSessionConfig().getUsers().forEach(user -> user.setVote(0));
			view.preUpdateAll();
		});
		
		showVotesButton = new Button();
		showVotesButton.addClickListener(e -> {
			view.getSessionConfig().setShowVotes(!view.getSessionConfig().isShowVotes());
			view.preUpdateAll();
		});
		
		setAlignItems(Alignment.END);
		add(nameField, clearVotesButton, showVotesButton);
		expand(nameField);
		setWidthFull();
	}
	
	/**
	 * 
	 * 
	 * @Created 09.07.2020 - 23:35:13
	 * @author KaesDingeling
	 */
	public void update() {
		if (view.getSessionUserData().isCanWriteName()) {
			if (nameField.isReadOnly()) {
				nameField.setReadOnly(false);
			}
		} else {
			if (!nameField.isReadOnly()) {
				nameField.setReadOnly(true);
			}
		}
		
		if (!StringUtils.equals(view.getSessionConfig().getName(), nameField.getValue())) {
			nameField.setValue(view.getSessionConfig().getName());
		}
		
		if (view.getSessionUserData().isCanReset()) {
			if (!clearVotesButton.isVisible()) {
				clearVotesButton.setVisible(true);
			}
		} else if (clearVotesButton.isVisible()) {
			clearVotesButton.setVisible(false);
		}
		
		if (view.getSessionUserData().isCanShowVotes()) {
			if (!showVotesButton.isVisible()) {
				showVotesButton.setVisible(true);
			}
			
			if (view.isShowValue()) {
				showVotesButton.setIcon(FontAwesomeSolid.EYE_SLASH.create());
				showVotesButton.setThemeName(ButtonVariant.LUMO_PRIMARY.getVariantName(), true);
			} else {
				showVotesButton.setIcon(FontAwesomeSolid.EYE.create());
				showVotesButton.removeThemeVariants(ButtonVariant.LUMO_PRIMARY);
			}
			
			if (view.isShowValue()) {
				if (showVotesButton.isEnabled()) {
					showVotesButton.setEnabled(false);
				}
			} else if (!showVotesButton.isEnabled()) {
				showVotesButton.setEnabled(true);
			}
		} else if (showVotesButton.isVisible()) {
			showVotesButton.setVisible(false);
		}
	}

	@Override
	public void updateLanguages() {
		nameField.setLabel(UITextUtils.text(View_LanguageParameters.Pocker.name_input));
		nameField.setPlaceholder(UITextUtils.text(ELanguageSuffix.Placeholder.getName(), View_LanguageParameters.Pocker.name_input));
		
		UIUtils.setToolTip(clearVotesButton, UITextUtils.text(View_LanguageParameters.Pocker.button_clear));
		UIUtils.setToolTip(showVotesButton, UITextUtils.text(View_LanguageParameters.Pocker.button_show));
	}
}