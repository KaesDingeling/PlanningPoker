package de.kaesdingeling.planning.poker.views;

import java.util.Date;

import org.apache.commons.collections4.CollectionUtils;
import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.Registration;

import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.data.models.PokerData;
import de.kaesdingeling.planning.poker.data.models.PokerUserData;
import de.kaesdingeling.planning.poker.services.SessionService;
import de.kaesdingeling.planning.poker.views.router.RMainLayout;

/**
 * 
 * @created 14.06.2020 - 21:01:50
 * @author KaesDingeling
 * @version 0.1
 */
@Route(value = VAdmin.URL, layout = RMainLayout.class)
public class VAdmin extends VerticalLayout {
	private static final long serialVersionUID = 6038772547835802058L;

	public static final String URL = "admin/test2";
	
	private Grid<PokerData> grid;
	
	@Autowired
	private SessionService service;
	
	private Registration updateListener;

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		grid = new Grid<PokerData>();
		
		updateListener = service.addUpdateListener(data -> {
			attachEvent.getUI().access(() -> refresh());
		});

		grid.addColumn(i -> i.getUuid()).setHeader("UUID");
		grid.addColumn(i -> i.getName()).setHeader("Name");
		grid.addColumn(i -> CollectionUtils.size(i.getOptions())).setHeader("Optionen");
		grid.addColumn(i -> CollectionUtils.size(i.getUsers())).setHeader("Teilnehmer");
		
		grid.setItemDetailsRenderer(new ComponentRenderer<Component, PokerData>(i -> {
			HorizontalLayout content = new HorizontalLayout();
			Grid<PokerUserData> gridUser = new Grid<PokerUserData>();

			gridUser.addColumn(u -> u.getSessionID()).setHeader("UUID");
			gridUser.addColumn(u -> u.getUsername()).setHeader("Name");
			gridUser.addColumn(u -> u.getVote()).setHeader("Auswahl");
			gridUser.addColumn(u -> new PrettyTime(UITextUtils.loadLocale()).format(new Date(u.getJoined()))).setHeader("Beigetretten");
			gridUser.setItems(i.getUsers());
			
			content.add(gridUser);
			
			return content;
		}));
		GridContextMenu<PokerData> contextMenu = grid.addContextMenu();
		
		contextMenu.addItem("Aktualisieren", e -> refresh());
		
		add(grid);
		
		refresh();
		
		super.onAttach(attachEvent);
	}
	
	@Override
	protected void onDetach(DetachEvent detachEvent) {
		if (updateListener != null) {
			updateListener.remove();
		}
		
		super.onDetach(detachEvent);
	}
	
	/**
	 * 
	 * 
	 * @Created 14.07.2020 - 21:15:57
	 * @author KaesDingeling
	 */
	public synchronized void refresh() {
		grid.setItems(service.getSessions());
	}
}