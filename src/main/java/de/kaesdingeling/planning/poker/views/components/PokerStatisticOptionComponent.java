package de.kaesdingeling.planning.poker.views.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import de.kaesdingeling.commons.components.fontawesome.icons.FontAwesomeSolid;
import de.kaesdingeling.planning.poker.data.models.PokerOptionData;
import lombok.Getter;

/**
 * 
 * @created 09.07.2020 - 23:07:36
 * @author KaesDingeling
 * @version 0.1
 */
public class PokerStatisticOptionComponent extends HorizontalLayout {
	private static final long serialVersionUID = 1121443558644393295L;
	
	// Layout
	private Label name;
	private Component value;
	
	// Data
	@Getter
	private PokerOptionData data;
	
	/**
	 * 
	 * @param data
	 */
	public PokerStatisticOptionComponent(PokerOptionData data) {
		super();
		
		this.data = data;
		
		name = new Label(data.getName());
		
		add(name);
	}
	
	/**
	 * 
	 * @param value
	 * @Created 09.07.2020 - 23:08:11
	 * @author KaesDingeling
	 */
	public void update(Integer value) {
		if (this.value != null) {
			remove(this.value);
		}
		
		if (value != null) {
			this.value = new Label(String.valueOf(value));
		} else {
			this.value = FontAwesomeSolid.QUESTION_CIRCLE.create();
		}
		
		if (this.value != null) {
			add(this.value);
		}
	}
}