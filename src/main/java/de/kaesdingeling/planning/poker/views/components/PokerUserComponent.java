package de.kaesdingeling.planning.poker.views.components;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.google.common.collect.Lists;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.planning.poker.data.models.PokerUserData;
import de.kaesdingeling.planning.poker.views.VPoker;
import lombok.Getter;

/**
 * 
 * @created 18.06.2020 - 23:52:51
 * @author KaesDingeling
 * @version 0.1
 */
public class PokerUserComponent extends VerticalLayout implements HasLanguage {
	private static final long serialVersionUID = -7561561160218300731L;
	
	// Data
	@Getter
	private VPoker view;
	
	/**
	 * 
	 * @param view
	 */
	public PokerUserComponent(VPoker view) {
		super();
		
		this.view = view;
		
		setSpacing(false);
		
		reset();
	}
	
	@Override
	public void updateLanguages() {
		getComponetItems().forEach(i -> i.updateLanguages());
	}
	
	/**
	 * 
	 * 
	 * @Created 19.06.2020 - 00:28:59
	 * @author KaesDingeling
	 */
	public void reset() {
		
		
		update();
	}
	
	/**
	 * 
	 * 
	 * @Created 19.06.2020 - 10:54:49
	 * @author KaesDingeling
	 */
	public synchronized void update() {
		List<PokerUserData> users = Lists.newArrayList();
		
		for (PokerUserData user : view.getSessionConfig().getUsers()) {
			if (user != null) {
				users.add(user);
			}
		}
		
		for (PokerUserItemComponent item : getComponetItems()) {
			if (CollectionUtils.containsAny(users, item.getData())) {
				item.update();
				users.remove(item.getData());
			} else {
				remove(item);
			}
		}
		
		for (PokerUserData user : users) {
			add(new PokerUserItemComponent(view, user));
		}
	}
	
	/**
	 * 
	 * @return
	 * @Created 19.06.2020 - 12:21:31
	 * @author KaesDingeling
	 */
	public List<PokerUserItemComponent> getComponetItems() {
		return getChildren()
				.filter(Objects::nonNull)
				.filter(c -> c instanceof PokerUserItemComponent)
				.map(c -> (PokerUserItemComponent) c)
				.collect(Collectors.toList());
	}
	
	/**
	 * 
	 * @return
	 * @Created 18.06.2020 - 23:56:26
	 * @author KaesDingeling
	 */
	public List<PokerUserData> getOptions() {
		return getComponetItems()
				.stream()
				.map(i -> i.getData())
				.collect(Collectors.toList());
	}
}