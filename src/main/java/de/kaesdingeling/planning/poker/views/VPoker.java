package de.kaesdingeling.planning.poker.views;

import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Sets;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;

import de.kaesdingeling.commons.components.fontawesome.icons.FontAwesomeSolid;
import de.kaesdingeling.commons.ui.utils.UIUtils;
import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.data.PokerNotificationConsumer;
import de.kaesdingeling.planning.poker.data.PokerUpdateConsumer;
import de.kaesdingeling.planning.poker.data.constants.CONSTANTS;
import de.kaesdingeling.planning.poker.data.language.parameters.View_LanguageParameters;
import de.kaesdingeling.planning.poker.data.models.PokerData;
import de.kaesdingeling.planning.poker.data.models.PokerUserData;
import de.kaesdingeling.planning.poker.dialogs.DJoinSession;
import de.kaesdingeling.planning.poker.utils.PokerUtils;
import de.kaesdingeling.planning.poker.utils.UIPokerUtils;
import de.kaesdingeling.planning.poker.views.components.PokerHeaderComponent;
import de.kaesdingeling.planning.poker.views.components.PokerOptionsComponent;
import de.kaesdingeling.planning.poker.views.components.PokerStatisticComponent;
import de.kaesdingeling.planning.poker.views.components.PokerUserComponent;
import de.kaesdingeling.planning.poker.views.router.RMainLayout;
import lombok.Getter;

/**
 * 
 * @created 14.06.2020 - 21:01:50
 * @author KaesDingeling
 * @version 0.1
 */
@Route(value = VPoker.URL, layout = RMainLayout.class)
public class VPoker extends VerticalLayout implements HasUrlParameter<String>, BeforeLeaveObserver, HasLanguage {
	private static final long serialVersionUID = -6614250507288878437L;
	
	public static final String URL = "poker";
	
	// Layout
	private Button leaveSessionButton;
	private VerticalLayout rootContent;
	private PokerHeaderComponent header;
	private HorizontalLayout mainContent;
	private PokerStatisticComponent results;
	
	private VerticalLayout content;
	private PokerOptionsComponent optionsContent;
	private PokerUserComponent userContent;
	
	// Data
	@Getter
	private PokerUserData sessionUserData;
	@Getter
	private PokerData sessionConfig;
	
	@Getter
	private boolean showValue = false;

	@Getter
	private Set<Consumer<VPoker>> updateTasks = Sets.newHashSet();

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		if (sessionConfig != null && sessionUserData == null) {
			sessionUserData = UIPokerUtils.findOrCreateUser(sessionConfig, attachEvent.getUI());
			
			header = new PokerHeaderComponent(this);
			optionsContent = new PokerOptionsComponent(this);
			userContent = new PokerUserComponent(this);
			
			results = new PokerStatisticComponent(this);
			
			content = new VerticalLayout();
			content.add(optionsContent, userContent);
			
			mainContent = new HorizontalLayout();
			mainContent.add(content, results);
			mainContent.setWidthFull();
			
			// Root-Content
			rootContent = new VerticalLayout();
			rootContent.add(header, mainContent);
			rootContent.setMaxWidth("800px");
			rootContent.setWidthFull();
			
			leaveSessionButton = new Button();
			leaveSessionButton.setIcon(FontAwesomeSolid.ARROW_CIRCLE_LEFT.create());
			leaveSessionButton.addThemeVariants(ButtonVariant.LUMO_LARGE);
			leaveSessionButton.getStyle().set("position", "fixed");
			leaveSessionButton.getStyle().set("left", "10px");
			leaveSessionButton.getStyle().set("top", "10px");
			leaveSessionButton.getStyle().set("margin", "0");
			leaveSessionButton.addClickListener(e -> attachEvent.getUI().navigate(VMain.class));
			
			setJustifyContentMode(JustifyContentMode.CENTER);
			setAlignItems(Alignment.CENTER);
			add(leaveSessionButton, rootContent);
			
			updateTasks.add(new PokerNotificationConsumer(UITextUtils.text(View_LanguageParameters.Pocker.user_join, getSessionUserData().getUsername())));
			preUpdateAll();
		} else {
			attachEvent.getUI().navigate(VMain.class);
		}
		
		updateLanguages();
		
		super.onAttach(attachEvent);
	}
	
	/**
	 * 
	 * 
	 * @Created 09.07.2020 - 01:38:52
	 * @author KaesDingeling
	 */
	public void preUpdateAll() {
		if (getSessionConfig() != null) {
			getSessionConfig().updatePermissions();
			
			updateTasks.add(new PokerUpdateConsumer());
			
			executeUpdateForAll(Sets.newHashSet(updateTasks));
		}
		
		updateTasks.clear();
	}
	
	/**
	 * 
	 * 
	 * @Created 10.07.2020 - 00:01:41
	 * @author KaesDingeling
	 */
	public void preUpdateExcludeOwn() {
		if (getSessionConfig() != null) {
			getSessionConfig().updatePermissions();
			
			updateTasks.add(new PokerUpdateConsumer());
			
			executeUpdateForAll(getSessionConfig().getUsers()
					.stream()
					.filter(user -> !user.equals(getSessionUserData()))
					.collect(Collectors.toSet()), Sets.newHashSet(updateTasks));
		}
		
		updateTasks.clear();
	}
	
	/**
	 * 
	 * @param updateTasks
	 * @Created 09.07.2020 - 01:38:13
	 * @author KaesDingeling
	 */
	private void executeUpdateForAll(Set<Consumer<VPoker>> updateTasks) {
		executeUpdateForAll(getSessionConfig().getUsers(), updateTasks);
	}
	
	/**
	 * 
	 * @param users
	 * @param updateTasks
	 * @Created 10.07.2020 - 00:00:43
	 * @author KaesDingeling
	 */
	private void executeUpdateForAll(Set<PokerUserData> users, Set<Consumer<VPoker>> updateTasks) {
		Iterator<PokerUserData> userIterator = users.iterator();
		
		while (userIterator.hasNext()) {
			PokerUserData user = userIterator.next();
			
			ExecutorService thread = Executors.newSingleThreadExecutor();
			thread.execute(() -> {
				user.executeOnUIs(ui -> ui.access(() -> {
					ui.getInternals().getActiveRouterTargetsChain()
							.stream()
							.filter(c -> c instanceof VPoker)
							.map(c -> (VPoker) c)
							.forEach(c -> {
								for (Consumer<VPoker> consumer : updateTasks) {
									consumer.accept(c);
								}
							});
				}));
			});
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 19.06.2020 - 13:10:01
	 * @author KaesDingeling
	 */
	public void update() {
		boolean noValuePresent = false;
		
		for (PokerUserData user : getSessionConfig().getUsers()) {
			if (user.getVote() <= 0) {
				noValuePresent = true;
				break;
			}
		}
		
		showValue = (getSessionConfig().isShowVotes() || !noValuePresent);
		
		optionsContent.update();
		userContent.update();
		results.update();
		header.update();
	}

	@Override
	public void setParameter(BeforeEvent event, String parameter) {
		UUID uuid = PokerUtils.parseID(parameter);
		
		if (uuid == null) {
			Notification notification = new Notification("", 10000, Position.BOTTOM_CENTER);
			
			notification.setText(UITextUtils.text(View_LanguageParameters.Pocker.unsupported));
			notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
			notification.open();
			
			event.forwardTo(VMain.class);
		} else if (uuid != null) {
			sessionConfig = PokerUtils.searchSessionById(uuid);
			
			if (sessionConfig == null) {
				Notification notification = new Notification("", 10000, Position.BOTTOM_CENTER);
				
				notification.setText(UITextUtils.text(View_LanguageParameters.Pocker.not_found, uuid.toString()));
				notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
				notification.open();
				
				event.forwardTo(VMain.class);
			} else {
				UUID userID = event.getUI().getSession().getAttribute(UUID.class);
				String userName = String.valueOf(event.getUI().getSession().getAttribute(CONSTANTS.COOKIE_USERNAME));
				
				if (userID == null || StringUtils.isBlank(userName) || StringUtils.equals(userName, "null")) {
					new DJoinSession(uuid.toString()).open();
					
					event.forwardTo(VMain.class);
				}
			}
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 09.07.2020 - 00:42:54
	 * @author KaesDingeling
	 */
	public void timeout() {
		if (getSessionUserData() != null && getSessionConfig() != null) {
			UIPokerUtils.addNotificationTask(getUpdateTasks(), View_LanguageParameters.Pocker.user_timeout, getSessionUserData().getUsername());
			
			removeSession();
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 19.06.2020 - 13:40:14
	 * @author KaesDingeling
	 */
	public void removeSession() {
		if (PokerUtils.removeUser(getSessionConfig(), getSessionUserData())) {
			preUpdateAll();
			
			sessionUserData = null;
			sessionConfig = null;
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 09.07.2020 - 22:41:50
	 * @author KaesDingeling
	 */
	public void yourKicked() {
		new PokerNotificationConsumer(UITextUtils.text(View_LanguageParameters.Pocker.user_your_kicked, getSessionUserData().getUsername()), NotificationVariant.LUMO_ERROR).accept(this);
		
		sessionConfig = null;
		
		getSessionUserData().executeOnUIs(ui -> ui.navigate(VMain.class));
	}
	
	/**
	 * 
	 * @param user
	 * @Created 09.07.2020 - 22:29:16
	 * @author KaesDingeling
	 */
	public boolean kickUser(PokerUserData user) {
		return UIPokerUtils.kickUser(sessionConfig, user, getUpdateTasks(), () -> preUpdateAll());
	}
	
	@Override
	protected void onDetach(DetachEvent detachEvent) {
		if (getSessionUserData() != null) {
			UIPokerUtils.addNotificationTask(getUpdateTasks(), View_LanguageParameters.Pocker.user_left, getSessionUserData().getUsername());
			
			getSessionUserData().getUis().remove(detachEvent.getUI());
		}
		
		super.onDetach(detachEvent);
	}

	@Override
	public void beforeLeave(BeforeLeaveEvent event) {
		if (getSessionUserData() != null) {
			removeSession();
		}
	}

	@Override
	public void updateLanguages() {
		if (leaveSessionButton != null) {
			UIUtils.setToolTip(leaveSessionButton, UITextUtils.text(View_LanguageParameters.Pocker.button_exit));
		}
	}
}