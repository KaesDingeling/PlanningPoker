package de.kaesdingeling.planning.poker.views.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;

import de.kaesdingeling.planning.poker.data.models.PokerOptionData;
import lombok.Getter;

/**
 * 
 * @created 10.07.2020 - 00:17:33
 * @author KaesDingeling
 * @version 0.1
 */
public class PokerOptionsOptionComponent extends Button {
	private static final long serialVersionUID = 757926922438835112L;
	
	// Data
	@Getter
	private PokerOptionsComponent options;
	@Getter
	private PokerOptionData data;

	/**
	 * 
	 * @param options
	 * @param data
	 */
	public PokerOptionsOptionComponent(PokerOptionsComponent options, PokerOptionData data) {
		super();
		
		this.options = options;
		this.data = data;
		
		getStyle().set("margin", "3px");
		
		setText(data.getName());
		addClickListener(e -> {
			if (options.getView().getSessionUserData() != null) {
				options.getView().getSessionUserData().setVote(data.getValue());
				options.getView().preUpdateAll();
			}
		});
	}
	
	/**
	 * 
	 * 
	 * @Created 10.07.2020 - 00:20:07
	 * @author KaesDingeling
	 */
	public void update() {
		if (getOptions().getView().getSessionUserData().getVote() == getData().getValue()) {
			setThemeName(ButtonVariant.LUMO_PRIMARY.getVariantName(), true);
		} else {
			removeThemeVariants(ButtonVariant.LUMO_PRIMARY);
		}
	}
	
	/*
	public HorizontalLayout createOption(PokerOptionData option) {
		
		Button button = new Button(option.getName());
		button.addClickListener(e -> {
			view.getSessionUserData().setVote(option.getValue());
			view.preUpdateAll();
		});

		HorizontalLayout layout = new HorizontalLayout();
		layout.getStyle().set("display", "inline-flex");
		layout.getStyle().set("margin", "0 5px");
		layout.add(button);
		
		return layout;
	}
	*/
}