package de.kaesdingeling.planning.poker.views.components;

import java.util.Optional;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import de.kaesdingeling.commons.components.fontawesome.FontAwesomeIcon;
import de.kaesdingeling.commons.components.fontawesome.icons.FontAwesomeSolid;
import de.kaesdingeling.commons.ui.utils.UIUtils;
import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.data.PokerNotificationConsumer;
import de.kaesdingeling.planning.poker.data.language.parameters.View_LanguageParameters;
import de.kaesdingeling.planning.poker.data.models.PokerOptionData;
import de.kaesdingeling.planning.poker.data.models.PokerUserData;
import de.kaesdingeling.planning.poker.views.VPoker;
import lombok.Getter;

/**
 * 
 * @created 19.06.2020 - 13:02:58
 * @author KaesDingeling
 * @version 0.1
 */
public class PokerUserItemComponent extends HorizontalLayout implements HasLanguage {
	private static final long serialVersionUID = 4160708248247054209L;
	
	// Layout
	private final Label username;
	private Component output;
	private Button remove;
	
	// Data
	private final VPoker view;
	@Getter
	private final PokerUserData data;
	
	/**
	 * 
	 * @param view
	 * @param data
	 */
	public PokerUserItemComponent(final VPoker view, final PokerUserData data) {
		super();
		
		this.view = view;
		this.data = data;
		
		username = new Label();
		username.setWidthFull();
		
		setJustifyContentMode(JustifyContentMode.CENTER);
		setAlignItems(Alignment.CENTER);
		setHeight("46px");
		setWidthFull();
		
		update();
	}
	
	/**
	 * 
	 * 
	 * @Created 19.06.2020 - 13:03:56
	 * @author KaesDingeling
	 */
	public synchronized void update() {
		removeAll();
		
		username.setText(getData().getUsername());
		
		if (view.isShowValue()) {
			final Optional<PokerOptionData> selectedOption = view.getSessionConfig()
					.getOptions()
					.stream()
					.filter(o -> (o.getValue() == getData().getVote()))
					.findFirst();
			
			if (selectedOption.isPresent()) {
				output = new Label(selectedOption.get().getName());
			} else {
				output = FontAwesomeSolid.QUESTION_CIRCLE.create();
			}
		} else {
			if (getData().getVote() > 0) {
				output = FontAwesomeSolid.CHECK_CIRCLE.create();
				((FontAwesomeIcon) output).getStyle().set("color", "var(--lumo-success-color)");
			} else {
				output = FontAwesomeSolid.QUESTION_CIRCLE.create();
			}
		}
		
		if (output != null && output instanceof FontAwesomeIcon) {
			((FontAwesomeIcon) output).getStyle().set("font-size", "1.2em");
			((FontAwesomeIcon) output).setSize("35px");
		}
		
		add(username);
		expand(username);
		
		if (output != null) {
			if (output instanceof HasStyle) {
				((HasStyle) output).getStyle().set("white-space", "nowrap");
			}
			
			add(output);
		}
		
		if (view.getSessionUserData() != null && view.getSessionUserData().isCanKick()) {
			if (!getData().getSessionID().equals(view.getSessionUserData().getSessionID())) {
				if (remove == null) {
					remove = new Button(FontAwesomeSolid.UNLINK.create());
					remove.addThemeVariants(ButtonVariant.LUMO_ERROR);
					remove.addClickListener(e -> {
						if (!view.kickUser(getData())) {
							new PokerNotificationConsumer(UITextUtils.text(View_LanguageParameters.Pocker.user_failed_to_kicked, getData().getUsername()), NotificationVariant.LUMO_ERROR).accept(view);
						}
					});
				}
				
				add(remove);
			} else if (remove != null) {
				UIUtils.removeToolTip(remove);
				remove = null;
			}
			
			if (remove == null && output != null && output instanceof HasStyle) {
				((HasStyle) output).getStyle().set("margin-right", "52px");
			}
		} else if (remove != null) {
			UIUtils.removeToolTip(remove);
			remove = null;
		}
		
		updateLanguages();
	}
	
	@Override
	protected void onDetach(final DetachEvent detachEvent) {
		if (remove != null) {
			UIUtils.removeToolTip(remove);
		}
		
		super.onDetach(detachEvent);
	}

	@Override
	public void updateLanguages() {
		if (remove != null) {
			UIUtils.setToolTip(remove, UITextUtils.text(View_LanguageParameters.Pocker.button_kick, getData().getUsername()));
		}
		
		if (output != null && output instanceof FontAwesomeIcon) {
			if (((FontAwesomeIcon) output).getStyle().has("color")) {
				UIUtils.setToolTip((FontAwesomeIcon) output, UITextUtils.text(View_LanguageParameters.Pocker.icon_check));
			} else {
				UIUtils.setToolTip((FontAwesomeIcon) output, UITextUtils.text(View_LanguageParameters.Pocker.icon_unknown));
			}
		}
	}
}