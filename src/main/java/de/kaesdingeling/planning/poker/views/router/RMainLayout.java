package de.kaesdingeling.planning.poker.views.router;

import java.util.Locale;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Sets;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.RouterLayout;

import de.kaesdingeling.commons.ui.utils.UIUtils;
import de.kaesdingeling.commons.ui.utils.components.LanguageChanger;
import de.kaesdingeling.commons.ui.utils.components.ThemeChanger;
import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguageChanger;
import de.kaesdingeling.commons.ui.utils.interfaces.HasThemeChanger;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.commons.utils.Random;
import de.kaesdingeling.commons.utils.language.TextUtils;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguageBundle;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.General_LanguageParameters;
import de.kaesdingeling.planning.poker.data.language.parameters.View_LanguageParameters;
import de.kaesdingeling.planning.poker.services.LanguageService;
import de.kaesdingeling.planning.poker.services.UIHealthService;

/**
 * 
 * @created 14.06.2020 - 13:09:00
 * @author KaesDingeling
 * @version 0.1
 */
@SuppressWarnings("unchecked")
@CssImport("./styles/shared-styles.css")
@CssImport(value = "./KD-UI-Utils/Components/vaadin-notification.css", themeFor = "vaadin-notification-container")
public class RMainLayout extends HorizontalLayout implements RouterLayout, HasThemeChanger, HasLanguageChanger, HasLanguage, HasDynamicTitle {
	private static final long serialVersionUID = -3238448124368421799L;
	
	private HorizontalLayout menu;
	private ThemeChanger themeChanger;
	private LanguageChanger languageChanger;

	@Autowired
	private UIHealthService uiHealthService;
	
	/**
	 * 
	 */
	public RMainLayout(LanguageService languageService) {
		super();
		
		setId(Random.string(4));
		
		// Theme
		themeChanger = new ThemeChanger(e -> updateLanguages());
		
		// Langauge
		Set<Locale> availableLocales = Sets.newHashSet();
		
		for (ILanguageBundle bundle : languageService.getBundles()) {
			availableLocales.add(bundle.getPrimaryLocale());
		}
		
		languageChanger = new LanguageChanger(
				i -> TextUtils.text(ELanguageSuffix.Label.getName(), i, General_LanguageParameters.Lang.lang),
				i -> TextUtils.text(ELanguageSuffix.Url.getName(), i, General_LanguageParameters.Lang.lang));
		languageChanger.setItems(availableLocales);
		languageChanger.getStyle().set("margin-left", "var(--lumo-space-s)");
		
		menu = new HorizontalLayout();
		menu.getStyle().set("background-color", "var(--lumo-shade-5pct");
		menu.getStyle().set("border-top-right-radius", "5px");
		menu.getStyle().set("padding", "2px 6px");
		menu.getStyle().set("position", "fixed");
		menu.getStyle().set("z-index", "100");
		menu.getStyle().set("bottom", "0");
		menu.getStyle().set("left", "0");
		menu.setPadding(false);
		menu.setMargin(false);
		menu.add(themeChanger, languageChanger);
		
		setSpacing(false);
		setSizeFull();
		add(menu);
	}

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		languageChanger.setValueSilent(attachEvent.getUI().getLocale());
		
		UIUtils.checkOSTheme();
		UIUtils.checkOSLanguage();
		UIUtils.checkOSCurrency();
		
		uiHealthService.addUI(attachEvent.getUI());
		
		super.onAttach(attachEvent);
	}

	@Override
	public ThemeChanger getThemeChanger() {
		return themeChanger;
	}

	@Override
	public LanguageChanger getLanguageChanger() {
		return languageChanger;
	}
	
	@Override
	protected void onDetach(DetachEvent detachEvent) {
		uiHealthService.removeUI(detachEvent.getUI());
		
		super.onDetach(detachEvent);
	}
	
	@ClientCallable
	public void detach() {
		UI.getCurrent().close();
	}

	@Override
	public void updateLanguages() {
		if (themeChanger.isDark()) {
			themeChanger.setText(UITextUtils.text(General_LanguageParameters.Color.light));
		} else {
			themeChanger.setText(UITextUtils.text(General_LanguageParameters.Color.dark));
		}

		UIUtils.setToolTip(themeChanger, UITextUtils.text(ELanguageSuffix.Descrption.getName(), General_LanguageParameters.Color.color));
		UIUtils.setToolTip(languageChanger, UITextUtils.text(ELanguageSuffix.Descrption.getName(), General_LanguageParameters.Lang.lang));
	}

	@Override
	public String getPageTitle() {
		return UITextUtils.text(View_LanguageParameters.Main.main);
	}
}