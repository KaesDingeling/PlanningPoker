package de.kaesdingeling.planning.poker.views.components;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Maps;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguage;
import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.planning.poker.data.language.parameters.View_LanguageParameters;
import de.kaesdingeling.planning.poker.data.models.PokerOptionData;
import de.kaesdingeling.planning.poker.data.models.PokerUserData;
import de.kaesdingeling.planning.poker.views.VPoker;

/**
 * 
 * @created 09.07.2020 - 23:13:10
 * @author KaesDingeling
 * @version 0.1
 */
public class PokerStatisticComponent extends VerticalLayout implements HasLanguage {
	private static final long serialVersionUID = -4829994485160213640L;

	private H2 title;
	private H2 result;
	
	private VPoker view;
	
	/**
	 * 
	 * @param view
	 */
	public PokerStatisticComponent(VPoker view) {
		super();
		
		this.view = view;
		
		title = new H2();
		
		add(title);
		
		for (PokerOptionData option : view.getSessionConfig().getOptions()) {
			add(new PokerStatisticOptionComponent(option));
		}

		result = new H2();
		
		add(result);
	}
	
	/**
	 * 
	 * 
	 * @Created 09.07.2020 - 23:05:09
	 * @author KaesDingeling
	 */
	public void update() {
		if (view.isShowValue()) {
			result.setVisible(true);
			
			double selectedValues = 0.00d;
			
			Map<PokerOptionData, Integer> selectedOption = Maps.newHashMap();
			
			for (PokerOptionData option : getOptions()) {
				selectedOption.put(option, 0);
			}
			
			for (PokerUserData user : view.getSessionConfig().getUsers()) {
				selectedOption.entrySet().stream()
						.filter(o -> o.getKey().getValue() == user.getVote())
						.forEach(o -> o.setValue(o.getValue() + 1));
				
				if (user.getVote() > 0) {
					selectedValues += user.getVote();
				}
			}
			
			for (PokerStatisticOptionComponent resultOption : getComponetItems()) {
				if (selectedOption.get(resultOption.getData()) > 0) {
					if (!resultOption.isVisible()) {
						resultOption.setVisible(true);
					}
					
					resultOption.update(selectedOption.get(resultOption.getData()));
				} else if (resultOption.isVisible()) {
					resultOption.setVisible(false);
				}
			}
			
			double value = (selectedValues / CollectionUtils.size(view.getSessionConfig().getUsers()));
			
			String output = String.valueOf((Math.round(value * 100) / 100.00));
			
			if (StringUtils.endsWith(output, ".0")) {
				output = StringUtils.replace(output, ".0", "");
			}
			
			result.setText(output);
		} else {
			for (PokerStatisticOptionComponent resultOption : getComponetItems()) {
				resultOption.setVisible(false);
			}
			
			result.setVisible(false);
		}
	}
	
	/**
	 * 
	 * @return
	 * @Created 19.06.2020 - 12:21:31
	 * @author KaesDingeling
	 */
	public List<PokerStatisticOptionComponent> getComponetItems() {
		return getChildren()
				.filter(Objects::nonNull)
				.filter(c -> c instanceof PokerStatisticOptionComponent)
				.map(c -> (PokerStatisticOptionComponent) c)
				.collect(Collectors.toList());
	}
	
	/**
	 * 
	 * @return
	 * @Created 18.06.2020 - 23:56:26
	 * @author KaesDingeling
	 */
	public List<PokerOptionData> getOptions() {
		return getComponetItems()
				.stream()
				.map(i -> i.getData())
				.collect(Collectors.toList());
	}

	@Override
	public void updateLanguages() {
		title.setText(UITextUtils.text(View_LanguageParameters.Pocker.statistics));
	}
}