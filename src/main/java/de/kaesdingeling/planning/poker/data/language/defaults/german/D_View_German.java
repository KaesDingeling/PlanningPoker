package de.kaesdingeling.planning.poker.data.language.defaults.german;

import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguageEntry;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguagePackage;
import de.kaesdingeling.commons.utils.language.models.LanguageEntry;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguagePrefix;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.Dialog_LanguageParameters;
import de.kaesdingeling.planning.poker.data.language.parameters.View_LanguageParameters;

/**
 * 
 * @created 14.06.2020 - 13:53:44
 * @author KaesDingeling
 * @version 0.1
 */
public class D_View_German implements ILanguagePackage {
	
	private ILanguageEntry entry;
	
	@Override
	public ILanguageEntry getEntry() {
		if (entry == null) {
			entry = LanguageEntry.create(ELanguagePrefix.View.getName());
			
			// Main
			addEntry(View_LanguageParameters.Main.main, "Planungs Poker");
			addEntry(View_LanguageParameters.Main.main, "Willkommen beim Planungspoker! Agile Online-, virtuelle und ko-lokalisierte Teams nutzen diese Anwendung während ihrer Planungs-/Zeige-Sitzungen, um effektiv Punkte für Stories zu kommunizieren.", ELanguageSuffix.Descrption.getName());
			
			addEntry(View_LanguageParameters.Main.button_create, "Erstellen");
			addEntry(View_LanguageParameters.Main.button_join, "Beitreten");
			
			
			// Poker
			addEntry(View_LanguageParameters.Pocker.pocker, View_LanguageParameters.Main.main);

			addEntry(View_LanguageParameters.Pocker.button_exit, "Verlassen");
			addEntry(View_LanguageParameters.Pocker.button_kick, "{0} aus der Sitzung entfernen");
			addEntry(View_LanguageParameters.Pocker.button_show, "Ergebnis ansehen");
			addEntry(View_LanguageParameters.Pocker.button_clear, "Zurücksetzten");
			
			addEntry(View_LanguageParameters.Pocker.icon_unknown, "Noch keine Auswahl getroffen");
			addEntry(View_LanguageParameters.Pocker.icon_check, "Auswahl getroffen");
			
			addEntry(View_LanguageParameters.Pocker.name_input, "Story Beschreibung");
			addEntry(View_LanguageParameters.Pocker.name_input, ELanguageSuffix.Placeholder.getName(), UITextUtils.textLink(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName()));
			addEntry(View_LanguageParameters.Pocker.statistics, "Statistik");
			addEntry(View_LanguageParameters.Pocker.average, "Durschnitt");
			
			addEntry(View_LanguageParameters.Pocker.unsupported, "Eingabe ungültig");
			addEntry(View_LanguageParameters.Pocker.not_found, "Sitzung \"{0}\" nicht gefunden");
			addEntry(View_LanguageParameters.Pocker.user_join, "{0} ist der Sitzung beigetreten");
			addEntry(View_LanguageParameters.Pocker.user_left, "{0} hat die Sitzung verlassen");
			addEntry(View_LanguageParameters.Pocker.user_timeout, "{0} hat eine Zeitüberschreitung");
			addEntry(View_LanguageParameters.Pocker.user_was_kicked, "{0} wurde aus der Sitzung entfernt");
			addEntry(View_LanguageParameters.Pocker.user_your_kicked, "Du wurdest aus der Sitzung entfernt");
			addEntry(View_LanguageParameters.Pocker.user_failed_to_kicked, "Entfernen von {0} ist fehlgeschlagen");
		}
		
		return entry;
	}
}