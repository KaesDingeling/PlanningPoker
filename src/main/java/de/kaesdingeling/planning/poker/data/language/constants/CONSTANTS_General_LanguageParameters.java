package de.kaesdingeling.planning.poker.data.language.constants;

import de.kaesdingeling.planning.poker.data.language.enums.ELanguagePrefix;

/**
 * 
 * @created 14.06.2020 - 15:47:13
 * @author KaesDingeling
 * @version 0.1
 */
public class CONSTANTS_General_LanguageParameters {
	
	public static final String prefix = ELanguagePrefix.General.getName();
	
	public static class Lang {
		public static final String lang = "lang";
	}
	
	public static class Color {
		public static final String color = "color";

		public static final String dark = "dark";
		public static final String light = "light";
	}
}