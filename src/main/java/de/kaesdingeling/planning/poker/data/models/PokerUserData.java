package de.kaesdingeling.planning.poker.data.models;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

import com.google.common.collect.Sets;
import com.vaadin.flow.component.UI;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @created 18.06.2020 - 22:55:51
 * @author KaesDingeling
 * @version 0.1
 */
@Data
@Builder
@EqualsAndHashCode(of = "sessionID")
public class PokerUserData implements Serializable {
	private static final long serialVersionUID = 2498785882307596307L;
	
	private UUID sessionID;
	private String username;
	private int vote; // Aktuelle ausgewählte Option
	
	@Builder.Default
	private Set<UI> uis = Sets.newConcurrentHashSet();

	@Builder.Default
	private boolean canWriteName = false;
	
	@Builder.Default
	private boolean canShowVotes = false;
	
	@Builder.Default
	private boolean canReset = false;
	
	@Builder.Default
	private boolean canKick = false;
	
	private long joined;
	
	/**
	 * 
	 * @param executor
	 * @Created 05.09.2020 - 12:54:57
	 * @author KaesDingeling
	 */
	public void executeOnUIs(final Consumer<UI> executor) {
		for (UI ui : uis) {
			executor.accept(ui);
		}
	}
}