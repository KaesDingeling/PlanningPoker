package de.kaesdingeling.planning.poker.data.language.parameters;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageParameter;
import de.kaesdingeling.commons.utils.language.models.LanguageParameter;
import de.kaesdingeling.planning.poker.data.language.constants.CONSTANTS_Dialog_LanguageParameters;

/**
 * 
 * @created 14.06.2020 - 15:47:10
 * @author KaesDingeling
 * @version 0.1
 */
public class Dialog_LanguageParameters {
	
	public static class JoinSession {
		public static final ILanguageParameter join_session = LanguageParameter.get(CONSTANTS_Dialog_LanguageParameters.prefix, CONSTANTS_Dialog_LanguageParameters.JoinSession.join_session);
		
		public static final ILanguageParameter id = LanguageParameter.get(join_session, CONSTANTS_Dialog_LanguageParameters.JoinSession.id);
		public static final ILanguageParameter username = LanguageParameter.get(join_session, CONSTANTS_Dialog_LanguageParameters.JoinSession.username);
		public static final ILanguageParameter join = LanguageParameter.get(join_session, CONSTANTS_Dialog_LanguageParameters.JoinSession.join);
	}
	
	public static class CreateSession {
		public static final ILanguageParameter create_session = LanguageParameter.get(CONSTANTS_Dialog_LanguageParameters.prefix, CONSTANTS_Dialog_LanguageParameters.CreateSession.create_session);
		
		public static final ILanguageParameter username = LanguageParameter.get(create_session, CONSTANTS_Dialog_LanguageParameters.CreateSession.username);
		
		public static class Config {
			public static final ILanguageParameter config = LanguageParameter.get(create_session, CONSTANTS_Dialog_LanguageParameters.CreateSession.Config.config);
			
			public static final ILanguageParameter input_name = LanguageParameter.get(config, CONSTANTS_Dialog_LanguageParameters.CreateSession.Config.input_name);
			public static final ILanguageParameter input_value = LanguageParameter.get(config, CONSTANTS_Dialog_LanguageParameters.CreateSession.Config.input_value);
		}
		
		public static class Permission {
			public static final ILanguageParameter permission = LanguageParameter.get(create_session, CONSTANTS_Dialog_LanguageParameters.CreateSession.Permission.permission);

			public static final ILanguageParameter all_can_write_name = LanguageParameter.get(permission, CONSTANTS_Dialog_LanguageParameters.CreateSession.Permission.all_can_write_name);
			public static final ILanguageParameter all_can_show_votes = LanguageParameter.get(permission, CONSTANTS_Dialog_LanguageParameters.CreateSession.Permission.all_can_show_votes);
			public static final ILanguageParameter all_can_reset = LanguageParameter.get(permission, CONSTANTS_Dialog_LanguageParameters.CreateSession.Permission.all_can_reset);
			public static final ILanguageParameter all_can_kick = LanguageParameter.get(permission, CONSTANTS_Dialog_LanguageParameters.CreateSession.Permission.all_can_kick);
		}
		
		public static final ILanguageParameter create = LanguageParameter.get(create_session, CONSTANTS_Dialog_LanguageParameters.CreateSession.create);
	}
}