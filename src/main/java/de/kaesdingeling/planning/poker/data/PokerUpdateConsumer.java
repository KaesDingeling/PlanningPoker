package de.kaesdingeling.planning.poker.data;

import java.util.function.Consumer;

import de.kaesdingeling.planning.poker.views.VPoker;

/**
 * 
 * @created 09.07.2020 - 01:17:58
 * @author KaesDingeling
 * @version 0.1
 */
public class PokerUpdateConsumer implements Consumer<VPoker> {

	@Override
	public void accept(VPoker t) {
		t.update();
	}
}