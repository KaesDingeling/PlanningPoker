package de.kaesdingeling.planning.poker.data.language.parameters;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageParameter;
import de.kaesdingeling.commons.utils.language.models.LanguageParameter;
import de.kaesdingeling.planning.poker.data.language.constants.CONSTANTS_General_LanguageParameters;

/**
 * 
 * @created 14.06.2020 - 15:47:10
 * @author KaesDingeling
 * @version 0.1
 */
public class General_LanguageParameters {
	
	public static class Lang {
		public static final ILanguageParameter lang = LanguageParameter.get(CONSTANTS_General_LanguageParameters.prefix, CONSTANTS_General_LanguageParameters.Lang.lang);
	}
	
	public static class Color {
		public static final ILanguageParameter color = LanguageParameter.get(CONSTANTS_General_LanguageParameters.prefix, CONSTANTS_General_LanguageParameters.Color.color);

		public static final ILanguageParameter dark = LanguageParameter.get(color, CONSTANTS_General_LanguageParameters.Color.dark);
		public static final ILanguageParameter light = LanguageParameter.get(color, CONSTANTS_General_LanguageParameters.Color.light);
	}
}