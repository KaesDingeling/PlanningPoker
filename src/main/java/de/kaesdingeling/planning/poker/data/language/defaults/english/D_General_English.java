package de.kaesdingeling.planning.poker.data.language.defaults.english;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageEntry;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguagePackage;
import de.kaesdingeling.commons.utils.language.models.LanguageEntry;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguagePrefix;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.General_LanguageParameters;

/**
 * 
 * @created 14.06.2020 - 13:53:44
 * @author KaesDingeling
 * @version 0.1
 */
public class D_General_English implements ILanguagePackage {
	
	private ILanguageEntry entry;
	
	@Override
	public ILanguageEntry getEntry() {
		if (entry == null) {
			entry = LanguageEntry.create(ELanguagePrefix.General.getName());

			// Lang
			addEntry(General_LanguageParameters.Lang.lang, "English");
			addEntry(General_LanguageParameters.Lang.lang, ELanguageSuffix.Descrption.getName(), "Select language");
			addEntry(General_LanguageParameters.Lang.lang, ELanguageSuffix.Url.getName(), "./images/english.png");
			
			// Color
			addEntry(General_LanguageParameters.Color.color, ELanguageSuffix.Descrption.getName(), "Select theme");
			addEntry(General_LanguageParameters.Color.dark, "Dark");
			addEntry(General_LanguageParameters.Color.light, "Light");
		}
		
		return entry;
	}
}