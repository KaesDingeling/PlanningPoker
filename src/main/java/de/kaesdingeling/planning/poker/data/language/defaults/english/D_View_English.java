package de.kaesdingeling.planning.poker.data.language.defaults.english;

import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguageEntry;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguagePackage;
import de.kaesdingeling.commons.utils.language.models.LanguageEntry;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguagePrefix;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.Dialog_LanguageParameters;
import de.kaesdingeling.planning.poker.data.language.parameters.View_LanguageParameters;

/**
 * 
 * @created 14.06.2020 - 13:53:44
 * @author KaesDingeling
 * @version 0.1
 */
public class D_View_English implements ILanguagePackage {
	
	private ILanguageEntry entry;
	
	@Override
	public ILanguageEntry getEntry() {
		if (entry == null) {
			entry = LanguageEntry.create(ELanguagePrefix.View.getName());
			
			// Main
			addEntry(View_LanguageParameters.Main.main, "Planning Poker");
			addEntry(View_LanguageParameters.Main.main, "Welcome to planning poker! Online, virtual and co-located agile teams use this application during their planning/pointing sessions to effectively communicate points for stories.", ELanguageSuffix.Descrption.getName());
			
			addEntry(View_LanguageParameters.Main.button_create, "Create");
			addEntry(View_LanguageParameters.Main.button_join, "Join");
			
			
			// Poker
			addEntry(View_LanguageParameters.Pocker.pocker, View_LanguageParameters.Main.main);

			addEntry(View_LanguageParameters.Pocker.button_exit, "Quit");
			addEntry(View_LanguageParameters.Pocker.button_kick, "{0} remove from session");
			addEntry(View_LanguageParameters.Pocker.button_show, "Show Votes");
			addEntry(View_LanguageParameters.Pocker.button_clear, "Reset");
			
			addEntry(View_LanguageParameters.Pocker.icon_unknown, "No selection made yet");
			addEntry(View_LanguageParameters.Pocker.icon_check, "Selection made");
			
			addEntry(View_LanguageParameters.Pocker.name_input, "Story Description");
			addEntry(View_LanguageParameters.Pocker.name_input, ELanguageSuffix.Placeholder.getName(), UITextUtils.textLink(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName()));
			addEntry(View_LanguageParameters.Pocker.statistics, "Statistics");
			addEntry(View_LanguageParameters.Pocker.average, "Average");
			
			addEntry(View_LanguageParameters.Pocker.unsupported, "Input invalid");
			addEntry(View_LanguageParameters.Pocker.not_found, "Session \"{0}\" not found or invalid");
			addEntry(View_LanguageParameters.Pocker.user_join, "{0} join the session");
			addEntry(View_LanguageParameters.Pocker.user_left, "{0} left the session");
			addEntry(View_LanguageParameters.Pocker.user_timeout, "{0} has an timeout");
			addEntry(View_LanguageParameters.Pocker.user_was_kicked, "{0} was removed from this session");
			addEntry(View_LanguageParameters.Pocker.user_your_kicked, "You were removed from the session");
			addEntry(View_LanguageParameters.Pocker.user_failed_to_kicked, "Removing {0} is failed");
		}
		
		return entry;
	}
}