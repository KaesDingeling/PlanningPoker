package de.kaesdingeling.planning.poker.data.language.defaults.german;

import java.util.List;
import java.util.Locale;

import com.google.common.collect.Lists;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageBundle;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguagePackage;

/**
 * 
 * @created 14.06.2020 - 13:32:48
 * @author KaesDingeling
 * @version 0.1
 */
public class Default_German implements ILanguageBundle {
	
	public static final int VERSION = 14062020;

	@Override
	public int getVersion() {
		return VERSION;
	}

	@Override
	public boolean isDefaultLocale() {
		return false;
	}

	@Override
	public Locale getPrimaryLocale() {
		return Locale.GERMANY; // Deutschland
	}

	@Override
	public List<Locale> getSupportedLocales() {
		return Lists.newArrayList(
				Locale.GERMAN, 				// Deutsch
				getPrimaryLocale(), 		// Deutschland
				new Locale("de", "AT"), 	// Österreich
				new Locale("de", "LI"), 	// Lichtenstein
				new Locale("de", "LU"), 	// Luxenburg
				new Locale("de", "CH")); 	// Schweiz
	}

	@Override
	public List<ILanguagePackage> getAllPackages() {
		return Lists.newArrayList(new D_View_German(), new D_General_German(), new D_Dialog_German());
	}
}