package de.kaesdingeling.planning.poker.data.language.defaults.german;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageEntry;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguagePackage;
import de.kaesdingeling.commons.utils.language.models.LanguageEntry;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguagePrefix;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.General_LanguageParameters;

/**
 * 
 * @created 14.06.2020 - 13:53:44
 * @author KaesDingeling
 * @version 0.1
 */
public class D_General_German implements ILanguagePackage {
	
	private ILanguageEntry entry;
	
	@Override
	public ILanguageEntry getEntry() {
		if (entry == null) {
			entry = LanguageEntry.create(ELanguagePrefix.General.getName());
			
			// Lang
			addEntry(General_LanguageParameters.Lang.lang, "Deutsch");
			addEntry(General_LanguageParameters.Lang.lang, ELanguageSuffix.Descrption.getName(), "Sprache wählen");
			addEntry(General_LanguageParameters.Lang.lang, ELanguageSuffix.Url.getName(), "./images/german.png");
			
			// Color
			addEntry(General_LanguageParameters.Color.color, ELanguageSuffix.Descrption.getName(), "Design wählen");
			addEntry(General_LanguageParameters.Color.dark, "Dunkel");
			addEntry(General_LanguageParameters.Color.light, "Hell");
		}
		
		return entry;
	}
}