package de.kaesdingeling.planning.poker.data.models;

import java.io.Serializable;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.apache.logging.log4j.util.Strings;

import com.google.common.collect.Sets;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @created 14.06.2020 - 22:56:59
 * @author KaesDingeling
 * @version 0.1
 */
@Data
@Builder
@EqualsAndHashCode(of = "uuid")
public class PokerData implements Serializable {
	private static final long serialVersionUID = 244029135861227037L;

	private UUID uuid;
	
	@Builder.Default
	private String name = Strings.EMPTY;

	@Builder.Default
	private Set<PokerUserData> users = Sets.newConcurrentHashSet();
	
	@Builder.Default
	private Set<PokerOptionData> options = Sets.newConcurrentHashSet();

	@Builder.Default
	private boolean allCanWriteName = false;
	
	@Builder.Default
	private boolean allCanShowVotes = false;
	
	@Builder.Default
	private boolean allCanReset = false;
	
	@Builder.Default
	private boolean allCanKick = false;
	
	@Builder.Default
	private boolean showVotes = false;
	
	/**
	 * 
	 * 
	 * @Created 09.07.2020 - 21:19:49
	 * @author KaesDingeling
	 */
	public synchronized void updatePermissions() {
		if (isAllCanWriteName()) {
			users.stream()
				.filter(user -> !user.isCanWriteName())
				.forEach(user -> user.setCanWriteName(isAllCanWriteName()));
		}
		
		if (isAllCanShowVotes()) {
			users.stream()
				.filter(user -> !user.isCanShowVotes())
				.forEach(user -> user.setCanShowVotes(isAllCanShowVotes()));
		}
		
		if (isAllCanReset()) {
			users.stream()
				.filter(user -> !user.isCanReset())
				.forEach(user -> user.setCanReset(isAllCanReset()));
		}
		
		if (isAllCanKick()) {
			users.stream()
				.filter(user -> !user.isCanKick())
				.forEach(user -> user.setCanKick(isAllCanKick()));
		}
		
		if (!isAllCanShowVotes() || !isAllCanReset() || !isAllCanKick()) {
			final Optional<PokerUserData> firstUserOptional = findFirstSeenUser();

			if (firstUserOptional.isPresent()) {
				final PokerUserData firstUser = firstUserOptional.get();
				final boolean enableOption = true;
				
				if (!isAllCanWriteName()) {
					users.stream()
						.filter(user -> !user.getSessionID().equals(firstUser.getSessionID()))
						.filter(user -> user.isCanWriteName())
						.forEach(user -> user.setCanWriteName(isAllCanWriteName()));
				}
				
				if (!isAllCanShowVotes()) {
					users.stream()
						.filter(user -> !user.getSessionID().equals(firstUser.getSessionID()))
						.filter(user -> user.isCanShowVotes())
						.forEach(user -> user.setCanReset(isAllCanShowVotes()));
				}
				
				if (!isAllCanReset()) {
					users.stream()
						.filter(user -> !user.getSessionID().equals(firstUser.getSessionID()))
						.filter(user -> user.isCanReset())
						.forEach(user -> user.setCanReset(isAllCanShowVotes()));
				}
				
				if (!isAllCanKick()) {
					users.stream()
						.filter(user -> !user.getSessionID().equals(firstUser.getSessionID()))
						.filter(user -> user.isCanKick())
						.forEach(user -> user.setCanKick(isAllCanKick()));
				}

				firstUser.setCanWriteName(enableOption);
				firstUser.setCanShowVotes(enableOption);
				firstUser.setCanReset(enableOption);
				firstUser.setCanKick(enableOption);
			} else {
				if (!isAllCanWriteName()) {
					users.stream()
						.filter(user -> user.isCanWriteName())
						.forEach(user -> user.setCanWriteName(isAllCanWriteName()));
				}
				
				if (!isAllCanShowVotes()) {
					users.stream()
						.filter(user -> user.isCanShowVotes())
						.forEach(user -> user.setCanReset(isAllCanShowVotes()));
				}
				
				if (!isAllCanReset()) {
					users.stream()
						.filter(user -> user.isCanReset())
						.forEach(user -> user.setCanReset(isAllCanShowVotes()));
				}
				
				if (!isAllCanKick()) {
					users.stream()
						.filter(user -> user.isCanKick())
						.forEach(user -> user.setCanKick(isAllCanKick()));
				}
			}
		}
	}
	
	/**
	 * 
	 * @return
	 * @Created 09.07.2020 - 21:17:48
	 * @author KaesDingeling
	 */
	public Optional<PokerUserData> findFirstSeenUser() {
		return getUsers().stream()
				.sorted((v1, v2) -> ((Long) v1.getJoined()).compareTo(v2.getJoined()))
				.findFirst();
	}
}