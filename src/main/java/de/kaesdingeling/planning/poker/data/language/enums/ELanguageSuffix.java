package de.kaesdingeling.planning.poker.data.language.enums;

/**
 * 
 * @created 14.06.2020 - 15:48:36
 * @author KaesDingeling
 * @version 0.1
 */
public enum ELanguageSuffix {
	Url,
	Label,
	Descrption,
	Placeholder;
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 15:49:36
	 * @author KaesDingeling
	 */
	public String getName() {
		return name().toLowerCase();
	}
}