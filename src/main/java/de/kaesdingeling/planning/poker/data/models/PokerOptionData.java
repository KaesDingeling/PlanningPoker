package de.kaesdingeling.planning.poker.data.models;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @created 14.06.2020 - 22:58:09
 * @author KaesDingeling
 * @version 0.1
 */
@Data
@Builder
@EqualsAndHashCode(of = { "value", "name" })
public class PokerOptionData implements Serializable {
	private static final long serialVersionUID = -1131958221804762826L;
	
	@Builder.Default
	private int value = 0;
	private String name;
	@Builder.Default
	private int sort = 0;
}