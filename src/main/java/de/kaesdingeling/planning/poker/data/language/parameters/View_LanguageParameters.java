package de.kaesdingeling.planning.poker.data.language.parameters;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageParameter;
import de.kaesdingeling.commons.utils.language.models.LanguageParameter;
import de.kaesdingeling.planning.poker.data.language.constants.CONSTANTS_View_LanguageParameters;

/**
 * 
 * @created 14.06.2020 - 15:47:10
 * @author KaesDingeling
 * @version 0.1
 */
public class View_LanguageParameters {
	
	public static class Main {
		public static final ILanguageParameter main = LanguageParameter.get(CONSTANTS_View_LanguageParameters.prefix, CONSTANTS_View_LanguageParameters.Main.main);
		
		public static final ILanguageParameter button_create = LanguageParameter.get(main, CONSTANTS_View_LanguageParameters.Main.button_create);
		public static final ILanguageParameter button_join = LanguageParameter.get(main, CONSTANTS_View_LanguageParameters.Main.button_join);
	}
	
	public static class Pocker {
		public static final ILanguageParameter pocker = LanguageParameter.get(CONSTANTS_View_LanguageParameters.prefix, CONSTANTS_View_LanguageParameters.Pocker.pocker);

		public static final ILanguageParameter button_exit = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.button_exit);
		public static final ILanguageParameter button_kick = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.button_kick);
		public static final ILanguageParameter button_show = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.button_show);
		public static final ILanguageParameter button_clear = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.button_clear);
		
		public static final ILanguageParameter icon_unknown = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.icon_unknown);
		public static final ILanguageParameter icon_check = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.icon_check);

		public static final ILanguageParameter name_input = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.name_input);
		public static final ILanguageParameter statistics = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.statistics);
		public static final ILanguageParameter average = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.average);
		
		public static final ILanguageParameter unsupported = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.unsupported);
		public static final ILanguageParameter not_found = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.not_found);
		public static final ILanguageParameter user_join = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.user_join);
		public static final ILanguageParameter user_left = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.user_left);
		public static final ILanguageParameter user_timeout = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.user_timeout);
		public static final ILanguageParameter user_was_kicked = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.user_was_kicked);
		public static final ILanguageParameter user_your_kicked = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.user_your_kicked);
		public static final ILanguageParameter user_failed_to_kicked = LanguageParameter.get(pocker, CONSTANTS_View_LanguageParameters.Pocker.user_failed_to_kicked);
	}
}