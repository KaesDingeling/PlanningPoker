package de.kaesdingeling.planning.poker.data.language.defaults.english;

import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguageEntry;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguagePackage;
import de.kaesdingeling.commons.utils.language.models.LanguageEntry;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguagePrefix;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.Dialog_LanguageParameters;

/**
 * 
 * @created 14.06.2020 - 13:53:44
 * @author KaesDingeling
 * @version 0.1
 */
public class D_Dialog_English implements ILanguagePackage {
	
	private ILanguageEntry entry;
	
	@Override
	public ILanguageEntry getEntry() {
		if (entry == null) {
			entry = LanguageEntry.create(ELanguagePrefix.Dialog.getName());

			// Join-Session
			entry.addEntry(Dialog_LanguageParameters.JoinSession.join_session.getFull(), "Join session");
			
			addEntry(Dialog_LanguageParameters.JoinSession.id, "Session ID");
			addEntry(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName(), "Enter ...");
			addEntry(Dialog_LanguageParameters.JoinSession.username, "Username");
			addEntry(Dialog_LanguageParameters.JoinSession.username, ELanguageSuffix.Placeholder.getName(), UITextUtils.textLink(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName()));
			addEntry(Dialog_LanguageParameters.JoinSession.join, "Join");

			
			// Create-Session
			addEntry(Dialog_LanguageParameters.CreateSession.create_session, "Create session");
			
			addEntry(Dialog_LanguageParameters.CreateSession.username, "Username");
			addEntry(Dialog_LanguageParameters.CreateSession.username, ELanguageSuffix.Placeholder.getName(), UITextUtils.textLink(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName()));
			
			addEntry(Dialog_LanguageParameters.CreateSession.Config.config, "Option");
			addEntry(Dialog_LanguageParameters.CreateSession.Config.input_name, "Name");
			addEntry(Dialog_LanguageParameters.CreateSession.Config.input_name, ELanguageSuffix.Placeholder.getName(), UITextUtils.textLink(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName()));
			addEntry(Dialog_LanguageParameters.CreateSession.Config.input_value, "Value");
			addEntry(Dialog_LanguageParameters.CreateSession.Config.input_value, ELanguageSuffix.Placeholder.getName(), UITextUtils.textLink(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName()));

			addEntry(Dialog_LanguageParameters.CreateSession.Permission.permission, "Permission");
			addEntry(Dialog_LanguageParameters.CreateSession.Permission.all_can_write_name, "All can change description");
			addEntry(Dialog_LanguageParameters.CreateSession.Permission.all_can_show_votes, "All can show votes");
			addEntry(Dialog_LanguageParameters.CreateSession.Permission.all_can_reset, "All can reset votes");
			addEntry(Dialog_LanguageParameters.CreateSession.Permission.all_can_kick, "All can kick users");

			addEntry(Dialog_LanguageParameters.CreateSession.create, "Create");
		}
		
		return entry;
	}
}