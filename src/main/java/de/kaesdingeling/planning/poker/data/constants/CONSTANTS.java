package de.kaesdingeling.planning.poker.data.constants;

/**
 * 
 * @created 14.06.2020 - 20:08:09
 * @author KaesDingeling
 * @version 0.1
 */
public class CONSTANTS {
	
	public static final String COOKIE_USERNAME = "session-username";
	
	public static final int HEARTBEAT_INTERVAL = 45; // in sekunden
	
	public static final String DOUBLE_OUTPUT_FORMAT = "#.00";
}