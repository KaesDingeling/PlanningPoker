package de.kaesdingeling.planning.poker.data.language.constants;

import de.kaesdingeling.planning.poker.data.language.enums.ELanguagePrefix;

/**
 * 
 * @created 14.06.2020 - 15:47:13
 * @author KaesDingeling
 * @version 0.1
 */
public class CONSTANTS_View_LanguageParameters {
	
	public static final String prefix = ELanguagePrefix.View.getName();
	
	public static class Main {
		public static final String main = "main";
		
		public static final String button_create = "button.create";
		public static final String button_join = "button.join";
	}
	
	public static class Pocker {
		public static final String pocker = "pocker";
		
		public static final String button_exit = "button.exit";
		public static final String button_kick = "button.kick";
		public static final String button_show = "button.show";
		public static final String button_clear = "button.clear";
		
		public static final String icon_unknown = "icon.unknown";
		public static final String icon_check = "icon.check";

		public static final String name_input = "name.input";
		public static final String statistics = "statistics";
		public static final String average = "average";
		
		public static final String unsupported = "unsupported";
		public static final String not_found = "not.found";
		public static final String user_join = "user.join";
		public static final String user_left = "user.left";
		public static final String user_timeout = "user.timeout";
		public static final String user_was_kicked = "user.was.kicked";
		public static final String user_your_kicked = "user.your.kicked";
		public static final String user_failed_to_kicked = "user.failed.to.kicked";
	}
}