package de.kaesdingeling.planning.poker.data.language.defaults.english;

import java.util.List;
import java.util.Locale;

import com.google.common.collect.Lists;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageBundle;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguagePackage;

/**
 * 
 * @created 14.06.2020 - 13:32:48
 * @author KaesDingeling
 * @version 0.1
 */
public class Default_English implements ILanguageBundle {
	
	public static final int VERSION = 14062020;

	@Override
	public int getVersion() {
		return VERSION;
	}

	@Override
	public boolean isDefaultLocale() {
		return true;
	}

	@Override
	public Locale getPrimaryLocale() {
		return Locale.US; // USA
	}

	@Override
	public List<Locale> getSupportedLocales() {
		return Lists.newArrayList(
				Locale.ENGLISH, 		// English
				getPrimaryLocale()); 	// USA
	}

	@Override
	public List<ILanguagePackage> getAllPackages() {
		return Lists.newArrayList(new D_View_English(), new D_General_English(), new D_Dialog_English());
	}
}