package de.kaesdingeling.planning.poker.data;

import java.util.function.Consumer;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.notification.Notification.Position;

import de.kaesdingeling.planning.poker.views.VPoker;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @created 09.07.2020 - 01:17:58
 * @author KaesDingeling
 * @version 0.1
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PokerNotificationConsumer implements Consumer<VPoker> {
	
	private String message;
	private NotificationVariant themeVariants;
	
	/**
	 * 
	 * @param message
	 */
	public PokerNotificationConsumer(String message) {
		this(message, null);
	}

	@Override
	public void accept(VPoker t) {
		Notification notification = new Notification(getMessage(), 10000, Position.BOTTOM_CENTER);
		
		if (getThemeVariants() != null) {
			notification.addThemeVariants(getThemeVariants());
		}
		
		notification.open();
	}
}