package de.kaesdingeling.planning.poker.data.language.constants;

import de.kaesdingeling.planning.poker.data.language.enums.ELanguagePrefix;

/**
 * 
 * @created 14.06.2020 - 15:47:13
 * @author KaesDingeling
 * @version 0.1
 */
public class CONSTANTS_Dialog_LanguageParameters {
	
	public static final String prefix = ELanguagePrefix.Dialog.getName();
	
	public static class JoinSession {
		public static final String join_session = "join.session";
		
		public static final String id = "id";
		public static final String username = "username";
		public static final String join = "join";
	}
	
	public static class CreateSession {
		public static final String create_session = "create.session";

		public static final String username = "username";
		
		public static class Config {
			public static final String config = "config";
			
			public static final String input_name = "input.name";
			public static final String input_value = "input.value";
		}
		
		public static class Permission {
			public static final String permission = "permission";
			
			public static final String all_can_write_name = "all.can.write.name";
			public static final String all_can_show_votes = "all.can.show.votes";
			public static final String all_can_reset = "all.can.reset";
			public static final String all_can_kick = "all.can.kick";
		}
		
		public static final String create = "create";
	}
}