package de.kaesdingeling.planning.poker.data.language.defaults.german;

import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguageEntry;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguagePackage;
import de.kaesdingeling.commons.utils.language.models.LanguageEntry;
import de.kaesdingeling.planning.poker.data.language.constants.CONSTANTS_Dialog_LanguageParameters;
import de.kaesdingeling.planning.poker.data.language.enums.ELanguageSuffix;
import de.kaesdingeling.planning.poker.data.language.parameters.Dialog_LanguageParameters;

/**
 * 
 * @created 14.06.2020 - 13:53:44
 * @author KaesDingeling
 * @version 0.1
 */
public class D_Dialog_German implements ILanguagePackage {
	
	private ILanguageEntry entry;
	
	@Override
	public ILanguageEntry getEntry() {
		if (entry == null) {
			entry = LanguageEntry.create(CONSTANTS_Dialog_LanguageParameters.prefix);

			// Join-Session
			addEntry(Dialog_LanguageParameters.JoinSession.join_session, "Session beitreten");
			addEntry(Dialog_LanguageParameters.JoinSession.id, "Session ID");
			addEntry(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName(), "Eingabe ...");
			addEntry(Dialog_LanguageParameters.JoinSession.username, "Benutzername");
			addEntry(Dialog_LanguageParameters.JoinSession.username, ELanguageSuffix.Placeholder.getName(), UITextUtils.textLink(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName()));
			addEntry(Dialog_LanguageParameters.JoinSession.join, "Beitreten");

			
			// Create-Session
			addEntry(Dialog_LanguageParameters.CreateSession.create_session, "Session erstellen");
			
			addEntry(Dialog_LanguageParameters.CreateSession.username, "Benutzername");
			addEntry(Dialog_LanguageParameters.CreateSession.username, ELanguageSuffix.Placeholder.getName(), UITextUtils.textLink(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName()));
			
			addEntry(Dialog_LanguageParameters.CreateSession.Config.config, "Optionen");
			addEntry(Dialog_LanguageParameters.CreateSession.Config.input_name, "Name");
			addEntry(Dialog_LanguageParameters.CreateSession.Config.input_name, ELanguageSuffix.Placeholder.getName(), UITextUtils.textLink(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName()));
			addEntry(Dialog_LanguageParameters.CreateSession.Config.input_value, "Wert");
			addEntry(Dialog_LanguageParameters.CreateSession.Config.input_value, ELanguageSuffix.Placeholder.getName(), UITextUtils.textLink(Dialog_LanguageParameters.JoinSession.id, ELanguageSuffix.Placeholder.getName()));
			
			addEntry(Dialog_LanguageParameters.CreateSession.Permission.permission, "Berechtigungen");
			addEntry(Dialog_LanguageParameters.CreateSession.Permission.all_can_write_name, "Jeder kann die Beschreibung ändern");
			addEntry(Dialog_LanguageParameters.CreateSession.Permission.all_can_show_votes, "Jeder kann die Auswahl sichtbar machen");
			addEntry(Dialog_LanguageParameters.CreateSession.Permission.all_can_reset, "Jeder kann Auswahl zurücksetzten");
			addEntry(Dialog_LanguageParameters.CreateSession.Permission.all_can_kick, "Jeder kann andere Entfernen");

			addEntry(Dialog_LanguageParameters.CreateSession.create, "Erstellen");
		}
		
		return entry;
	}
}