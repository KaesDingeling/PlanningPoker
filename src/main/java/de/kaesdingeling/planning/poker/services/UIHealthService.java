package de.kaesdingeling.planning.poker.services;

import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;
import com.vaadin.flow.component.UI;

import de.kaesdingeling.commons.utils.enums.ELogState;
import de.kaesdingeling.planning.poker.data.constants.CONSTANTS;
import de.kaesdingeling.planning.poker.views.VPoker;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class UIHealthService {
	
	@Getter
	private Set<UI> activeUIs = Sets.newConcurrentHashSet();
	
	/**
	 * 
	 * @param ui
	 * @Created 08.07.2020 - 23:53:08
	 * @author KaesDingeling
	 */
	public synchronized void addUI(UI ui) {
		activeUIs.add(ui);
	}
	
	/**
	 * 
	 * @param ui
	 * @return
	 * @Created 08.07.2020 - 23:53:34
	 * @author KaesDingeling
	 */
	public synchronized boolean removeUI(UI ui) {
		return activeUIs.remove(ui);
	}
	
	@Scheduled(cron = "*/20 * * * * *")
	public synchronized void check() {
		Iterator<UI> iterator = getActiveUIs().iterator();
		
		while (iterator.hasNext()) {
			UI ui = iterator.next();
			
			if (!ui.isClosing()) {
				long lastHeartbeatSeconds = TimeUnit.MILLISECONDS.toSeconds((System.currentTimeMillis() - ui.getInternals().getLastHeartbeatTimestamp()));
				
				if (lastHeartbeatSeconds > (CONSTANTS.HEARTBEAT_INTERVAL)) {
					log.info(ELogState.REMOVE + "inactive ui");
					ui.access(() -> {
						ui.getInternals().getActiveRouterTargetsChain()
								.stream()
								.filter(Objects::nonNull)
								.filter(c -> c instanceof VPoker)
								.map(c -> (VPoker) c)
								.forEach(c -> c.removeSession());
						ui.close();
					});
				}
			} else {
				iterator.remove();
			}
		}
	}
}