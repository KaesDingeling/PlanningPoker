package de.kaesdingeling.planning.poker.services;

import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.commons.ui.utils.language.interfaces.IUILanguageService;
import de.kaesdingeling.commons.utils.language.LanguageConfig;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguageBundle;
import de.kaesdingeling.commons.utils.language.models.TextContext;
import de.kaesdingeling.planning.poker.data.language.defaults.english.Default_English;
import de.kaesdingeling.planning.poker.data.language.defaults.german.Default_German;
import lombok.Getter;

/**
 * 
 * @created 14.06.2020 - 13:58:22
 * @author KaesDingeling
 * @version 0.1
 */
@Service
public class LanguageService implements IUILanguageService {
	private static final long serialVersionUID = 1964473908968070386L;
	
	@Getter
	private List<ILanguageBundle> bundles = Lists.newArrayList();
	
	@Override
	public void load() {
		bundles.clear();
		
		bundles.add(new Default_German());
		bundles.add(new Default_English());
	}
	
	@PostConstruct
	public void springInit() {
		init();
	}

	@Override
	public String getTranslation(final String key, final Locale locale, final Object... params) {
		//ResourceBundle.
		System.out.println("getTranslation: " + key + " - " + locale + " - " + params);
		
		final ResourceBundle translationBundle = ResourceBundle.getBundle("system", locale);
		
		System.out.println(translationBundle.getBaseBundleName());
		System.out.println(translationBundle.containsKey(key));
		
		if (translationBundle.containsKey(key)) {
			String message = translationBundle.getString(key);
			
			if (StringUtils.contains(message, "@{")) {
				final Matcher matcher = LanguageConfig.REFERENCE_PATTERN.matcher(message);
				
				while (matcher.find()) {
					final String textLink = matcher.group().replace("@{", "").replace("}@", "");
					final String linkPrefix = textLink.substring(0, textLink.indexOf(LanguageConfig.SEPARATOR));
					
					message = StringUtils.replace(message, key, getTranslation(linkPrefix, locale, params));
				}
			}
			
			if (ArrayUtils.isNotEmpty(params)) {
				message = MessageFormat.format(message, params);
			}
			
			return message;
		}
		
		return UITextUtils.text("label", locale, new TextContext(key), null, params);
	}
}