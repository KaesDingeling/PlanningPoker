package de.kaesdingeling.planning.poker.services;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.vaadin.flow.shared.Registration;

import de.kaesdingeling.planning.poker.data.models.PokerData;

/**
 * 
 * @created 18.06.2020 - 22:25:53
 * @author KaesDingeling
 * @version 0.1
 */
@Service
public class SessionService {
	
	private List<PokerData> sessions = Lists.newLinkedList();
	
	private List<Consumer<PokerData>> updateListener = Lists.newLinkedList();
	
	/**
	 * 
	 * @param session
	 * @return
	 * @Created 18.06.2020 - 22:53:17
	 * @author KaesDingeling
	 */
	public synchronized boolean addSession(final PokerData session) {
		List<PokerData> sessions = getSessions();
		
		if (!CollectionUtils.containsAny(sessions, session)) {
			if (sessions.add(session)) {
				for (Consumer<PokerData> listener : updateListener) {
					listener.accept(session);
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param session
	 * @return
	 * @Created 05.09.2020 - 15:22:16
	 * @author KaesDingeling
	 */
	public void createHistory(final PokerData session) {
		// TODO
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @Created 19.06.2020 - 12:37:56
	 * @author KaesDingeling
	 */
	public synchronized PokerData getSession(final UUID id) {
		if (id != null) {
			List<PokerData> sessions = getSessions();
			
			for (PokerData session : sessions) {
				if (session.getUuid().equals(id)) {
					return session;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param listener
	 * @return
	 * @Created 14.07.2020 - 21:59:22
	 * @author KaesDingeling
	 */
	public Registration addUpdateListener(final Consumer<PokerData> listener) {
		updateListener.add(listener);
		return () -> {
			updateListener.remove(listener);
		};
	}
	
	/**
	 * 
	 * @return
	 * @Created 18.06.2020 - 22:42:07
	 * @author KaesDingeling
	 */
	public synchronized List<PokerData> getSessions() {
		return sessions;
	}
}