package de.kaesdingeling.planning.poker.utils;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import com.vaadin.flow.component.UI;

import de.kaesdingeling.commons.ui.utils.language.UITextUtils;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguageParameter;
import de.kaesdingeling.planning.poker.data.PokerNotificationConsumer;
import de.kaesdingeling.planning.poker.data.constants.CONSTANTS;
import de.kaesdingeling.planning.poker.data.language.parameters.View_LanguageParameters;
import de.kaesdingeling.planning.poker.data.models.PokerData;
import de.kaesdingeling.planning.poker.data.models.PokerUserData;
import de.kaesdingeling.planning.poker.views.VPoker;

/**
 * 
 * @created 05.09.2020 - 13:05:52
 * @author KaesDingeling
 * @version 0.1
 */
public class UIPokerUtils {

	/**
	 * 
	 * @param sessionConfig
	 * @param ui
	 * @return
	 * @Created 05.09.2020 - 13:08:48
	 * @author KaesDingeling
	 */
	public static PokerUserData findOrCreateUser(PokerData config, UI ui) {
		final PokerUserData sessionUserData;
		
		final Optional<PokerUserData> sessionUserDataOptional = config.getUsers()
				.stream()
				.filter(user -> user.getSessionID().equals(ui.getSession().getAttribute(UUID.class)))
				.findAny();
		
		if (sessionUserDataOptional.isPresent()) {
			sessionUserData = sessionUserDataOptional.get();
		} else {
			sessionUserData = PokerUserData.builder()
					.sessionID(ui.getSession().getAttribute(UUID.class))
					.username(String.valueOf(ui.getSession().getAttribute(CONSTANTS.COOKIE_USERNAME)))
					.joined(System.currentTimeMillis())
					.build();
			
			config.getUsers().add(sessionUserData);
		}

		sessionUserData.getUis().add(ui);
		
		return sessionUserData;
	}
	
	/**
	 * 
	 * @param config
	 * @param user
	 * @param updateTasks
	 * @param preUpdateAll
	 * @return
	 * @Created 05.09.2020 - 13:11:43
	 * @author KaesDingeling
	 */
	public static boolean kickUser(PokerData config, PokerUserData user, Set<Consumer<VPoker>> updateTasks, Runnable preUpdateAll) {
		if (PokerUtils.removeUser(config, user)) {
			ExecutorService thread = Executors.newSingleThreadExecutor();
			thread.execute(() -> {
				user.executeOnUIs(ui -> ui.access(() -> {
					ui.getInternals().getActiveRouterTargetsChain()
							.stream()
							.filter(c -> c instanceof VPoker)
							.map(c -> (VPoker) c)
							.forEach(c -> c.yourKicked());
				}));
			});
			
			addNotificationTask(updateTasks, View_LanguageParameters.Pocker.user_was_kicked, user.getUsername());
			
			preUpdateAll.run();
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param updateTasks
	 * @param parameter
	 * @param values
	 * @return
	 * @Created 05.09.2020 - 13:16:52
	 * @author KaesDingeling
	 */
	public static boolean addNotificationTask(Set<Consumer<VPoker>> updateTasks, final ILanguageParameter parameter, final Object... values) {
		return addNotificationTask(updateTasks, UITextUtils.text(parameter, values));
	}
	
	/**
	 * 
	 * @param updateTasks
	 * @param message
	 * @return
	 * @Created 05.09.2020 - 13:15:43
	 * @author KaesDingeling
	 */
	public static boolean addNotificationTask(Set<Consumer<VPoker>> updateTasks, final String message) {
		return addUpdateTask(updateTasks, new PokerNotificationConsumer(message));
	}
	
	/**
	 * 
	 * @param updateTasks
	 * @param task
	 * @return
	 * @Created 05.09.2020 - 13:15:15
	 * @author KaesDingeling
	 */
	public static boolean addUpdateTask(Set<Consumer<VPoker>> updateTasks, final Consumer<VPoker> task) {
		if (task != null) {
			return updateTasks.add(task);
		}
		
		return false;
	}
}