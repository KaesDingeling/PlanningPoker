package de.kaesdingeling.planning.poker.utils;

import java.util.Set;
import java.util.UUID;

import com.google.common.collect.Sets;

import de.kaesdingeling.commons.utils.enums.ELogState;
import de.kaesdingeling.planning.poker.Application;
import de.kaesdingeling.planning.poker.data.models.PokerData;
import de.kaesdingeling.planning.poker.data.models.PokerOptionData;
import de.kaesdingeling.planning.poker.data.models.PokerUserData;
import de.kaesdingeling.planning.poker.services.SessionService;
import lombok.extern.log4j.Log4j2;

/**
 * 
 * @created 14.06.2020 - 22:55:49
 * @author KaesDingeling
 * @version 0.1
 */
@Log4j2
public class PokerUtils {
	
	/**
	 * 
	 * @param id
	 * @return
	 * @Created 18.06.2020 - 18:36:37
	 * @author KaesDingeling
	 */
	public static UUID parseID(String id) {
		try {
			return UUID.fromString(id);
		} catch (Exception e) {
			log.debug(ELogState.FAILED + "to parse id {}", id, e);
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param uuid
	 * @return
	 * @Created 14.06.2020 - 23:03:17
	 * @author KaesDingeling
	 */
	public static PokerData searchSessionById(UUID uuid) {
		return Application.getBean(SessionService.class).getSession(uuid);
	}
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 23:07:07
	 * @author KaesDingeling
	 */
	public static PokerData createSession() {
		Set<PokerOptionData> options = Sets.newCopyOnWriteArraySet();
		
		options.add(PokerOptionData.builder()
						.value(1)
						.name("Point 1")
						.build());
		options.add(PokerOptionData.builder()
						.value(3)
						.name("Point 3")
						.build());
		options.add(PokerOptionData.builder()
						.value(5)
						.name("Point 5")
						.build());
		options.add(PokerOptionData.builder()
						.value(8)
						.name("Point 8")
						.build());
		
		return PokerData.builder()
				.uuid(UUID.randomUUID())
				.options(options)
				.build();
	}
	
	/**
	 * 
	 * @param data
	 * @param user
	 * @return
	 * @Created 09.07.2020 - 22:26:28
	 * @author KaesDingeling
	 */
	public static boolean removeUser(PokerData data, PokerUserData user) {
		if (data != null && user != null) {
			if (!data.getUsers().contains(user)) {
				return true;
			} else {
				if (data.getUsers().remove(user)) {
					data.updatePermissions();
					return true;
				}
			}
		}
		
		return false;
	}
}