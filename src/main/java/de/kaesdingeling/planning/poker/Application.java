package de.kaesdingeling.planning.poker;

import java.util.Optional;
import java.util.Properties;
import java.util.UUID;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

import de.kaesdingeling.planning.poker.data.constants.CONSTANTS;
import de.kaesdingeling.planning.poker.data.models.PokerData;
import de.kaesdingeling.planning.poker.services.SessionService;
import de.kaesdingeling.planning.poker.utils.PokerUtils;

/**
 * 
 * @created 14.06.2020 - 12:53:31
 * @author KaesDingeling
 * @version 0.1
 */
@Push
@EnableScheduling
@SpringBootApplication
@Theme(themeClass = Lumo.class)
@PWA(name = "Planning Poker", shortName = "PP", description = "")
public class Application extends SpringBootServletInitializer implements AppShellConfigurator {
	private static final long serialVersionUID = -6449890335432270065L;
	
	public static ConfigurableApplicationContext springContext;

	/**
	 * 
	 * @param args
	 * @Created 14.06.2020 - 12:53:29
	 * @author KaesDingeling
	 */
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(Application.class);
		
		Properties properties = new Properties();
		properties.put("vaadin.heartbeatInterval", CONSTANTS.HEARTBEAT_INTERVAL);
		
		application.setDefaultProperties(properties);
		
		springContext = application.run(args);

		SessionService sessionService = getBean(SessionService.class);
		PokerData session = PokerUtils.createSession();
		session.setUuid(UUID.fromString("182d6b47-f75c-41af-9092-1069b74ab8e7"));
		sessionService.addSession(session);
	}
	
	/**
	 * 
	 * @param <B>
	 * @param type
	 * @return
	 * @Created 18.06.2020 - 22:49:42
	 * @author KaesDingeling
	 */
	public static <B> B getBean(Class<B> type) {
		return springContext.getBean(type);
	}
	
	@Override
	public void configurePage(final AppShellSettings settings) {
		//settings.addInlineWithContents(InitialPageSettings.Position.PREPEND, "window.onbeforeunload = function (e) { document.getElementById('" + getId().get() + "').$server.detach(); };", InitialPageSettings.WrapMode.JAVASCRIPT);
		
		final Optional<UI> foundUI = settings.getUi();
		
		if (foundUI.isPresent()) {
			final UI ui = foundUI.get();
			
			ui.getSession().setErrorHandler(error -> {
				System.out.println(error.getThrowable().getMessage());
				error.getThrowable().printStackTrace();
			});
			
			if (ui.getSession().getAttribute(UUID.class) == null) {
				ui.getSession().setAttribute(UUID.class, UUID.randomUUID());
			}
		}
    }
}